
# [ DEPRECATED ] v4 BASE Configuration

The v4 ussd abstracts all menus and API configurations into JSON format. This makes it trivial to add or modify menus and their content.

## Table of Contents

---

1. [Folder Structure](#example)
2. [Components and pages](#example2)
3. [Component configuration](#third-example)
4. [Prompts & handlers](#fourth-example)
5. [Page Configuration](#fourth-example)
6. [App Configuration](#fourth-example)
7. [Sample USSD](#fourth-example)

---

## 1. Folder Structure

The sample folder structure for a ussd app configuration is as shown below:

    api
        |___ api-mock
        |___ api-service
    core
        |___ cache
        |___ http
        |___ menu-handler
    log
    www
        |___ app_name@version
            |__ prompts
                    |__ full-statement-prompt.json
                    |__ balance-enquiry-prompt.json
            |__ pages
                    |__ base-page.json
                    |__ client-page.json
                    |__ agent-page.json
                    |__ account-enquiries-module.json
            |__ validators
                language.json
                config.json

---

## 2. Components and pages

A USSD app consists of `pages` and `prompts` as defined below:

`prompts`

A prompt is a single menu sequence consisting of multiple steps that ultimately end up with an API call to a backend service

    e.g a funds transfer component may consist of the following steps:
        1. Select the account to transfer from
        2. Select the account to transfer to
        3. Select the amount to transfer
        4. Confirm or Cancel the tranaction
`pages`

 A module can be thought of as a container that houses components or other pages.

    e.g we can have a client module that contains an account enquiries module and a funds transfer module. The account enquiries module is a nested module in this case and may contain a balance enquiry component and a full statement component.

    |__ client-module
        |__ account-enquiries
            |__ balance-enquiry-component
            |__ full-statement-component
        |__ funds-transfer

As you can see from above, pages can contain components, or pages or both. This makes organizing nested menus a bit easier to design and implement.

---

## 3. Component configuration

The component configuration is as shown below:

```json
    {
        "component-name"         : "string",
        "description"            : "string",
        "steps"                  : [
            {
                "initial-state"  : "string",
                "handler"        : "string",
                "prompt"         : "string",
                "type"           : "string",
                "canGoBack"      : "boolean",
                "nextStep"       : "string",
                "previousStep"   : "string",
                "hasOptions"     : "string",
                "options"        : "string",
                "inputValidation": "string",
                "identifier"     : "string",
                "errors"         : {
                    "0"          : "string"
                }
            },
        ],
        "api"                    : {
            "path"               : "string"
        }
    }
```

| Property  |  Accepted Values  | Description  | Examples   |
|-----------|---|---|---|
| component Name  | `String` | The name of the component   | funds-transfer-component  |
| Description  | `String`  | A brief description of the component   |  A generic Funds Transfer component |
| Steps  | `Array of Objects` | An array containing objects that represent an individual menu  | [ { ... individual menu configuration here } ]  |
| initial-state  | `String`  | This is the name of the menu's prompt that is used as a key in the language file object ( in this case, it is used as an Id )  |  funds_transfer_account_from_prompt |
| handler  | `String`  | This is the name of the menu's handler that handles the user input, performs validations and api calls and generates a menu based on the results  |  funds_transfer_account_from_handler |
| prompt  | `String`  | This is the name of the menu's prompt that is used as a key in the language file object  | funds_transfer_prompt  |
| type  |  `String` | This is the menu type  | select, input  |
| canGoBack  | `Boolean`  | This indicates whether navigation to a previous menu is enabled  | true or false  |
| allowNull  | `Boolean`  | This indicates that empty values in an input menu are allowed  | treu or false  |
| nextStep  |  `String` or `Boolean` | This is the name of the next menu's prompt that is used as a key in the language file object   | funds_transfer_account_to_prompt  |
| previousStep  | `String` or `Boolean`  | This is the name of the previous menu's prompt that is used as a key in the language file object  | funds_transfer_confirm_prompt or false  |
| hasOptions  |  `Boolean` | This indicates that the menu is a select menu and hence it has an options list  | true or false  |
| options  |  `String` | This is a string that is a key in the user data  that contains an array of objects for the options  | linkedAccounts  |
| inputValidation  | `String`  |  This is a string that contains pipe separated validation function names along with their arguments | `isNumber|isAmount:min=50`  |
| identifier  |  `String` or `Boolean` | This is the name of the variable that the user input value will be assigned to in the user data  | `accountFrom` |
| errors  | `Object`  |  This is an object that holds the error keys mapped to the position of the error on tha inputValidation string. Note that for select menus, there is only one key i.e. { "0": "" }  | { "0" : "funds_transfer_account_from_error_0"}  |
| api  | `Object`  |  This is an object that holds API properties for the component |   |
| path  | `String`   | This is the API endpoint path for the component | `/funds-transfer`  |

---

## 4. Prompts & handlers

`prompts`

A prompt state only fetches the prompt key and displays an initial menu prompt. It also includes the select options in tha case of a select menu, the previous option in case the `canGoBack` flag is set to true and ( enter s to skip ) in case of an input where `allowNull` flag is set to true.

`handlers`

A handler state considers user input, any set validations and api calls and based on the validation logic, displays the appropriate menu prompt.

---

## 5. Module Configuration

A module will always be displayed as a `select menu`.

The module configuration is very similar to a component with the only exception being the `children` key whereby the children of the module are listed and the `display` key whereby the module display options are set.

The children may be pages or components or a mix of both. This makes nesting menus very easy.

```json
{
    "module-name"            : "funds-transfer-module",
    "description"            : "",
    "children"               : [
        {
            "name"           : "funds-transfer-component",
            "label"          : "Funds Transfer",
            "enabled"        : true
        }
    ],
    "display"                : [
        {
            "initial-state"  : "funds_transfer_module_prompt",
            "handler"        : "funds_transfer_module_handler",
            "prompt"         : "funds_transfer_module_prompt",
            "type"           : "select",
            "canGoBack"      : true,
            "nextStep"       : false,
            "previousStep"   : false,
            "hasOptions"     : true,
            "options"        : false,
            "inputValidation": "isNumber",
            "identifier"     : false,
            "errors"         : {
                "0"          : "funds_transfer_module_prompt_error_0"
            }
        }
    ]
}
```
