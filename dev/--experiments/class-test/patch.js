function HarrysAwesomepatch () {

    //airtime
    const airtime         = [
        {
            name          : "airtime",
            text          : "please select a service provider",
            saveAs        : "provider",
            options       : [
                {
                    label : "Safaricom",
                    value : "safaricom"
                },
                {
                    label : "Airtel",
                    value : "airtel"
                }           
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount of airtime to purchase"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //bills
    const bills           = [
        {
            name          : "airtime",
            text          : "please select a service provider",
            saveAs        : "provider",
            options       : [
                {
                    label : "Safaricom",
                    value : "safaricom"
                },
                {
                    label : "Airtel",
                    value : "airtel"
                }           
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //enquiries
    const ministatement   = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]
    const fullstatement   = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]
    const balance         = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //funds transfer
    const ftWallet2wallet = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]
    const ftWallet2core   = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]
    const ftCore2wallet   = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]
    const ftCore2Core     = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //withdrawal
    const withdrawal      = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //deposit
    const deposit         = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //atm cards
    const atm             = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //cheques
    const cheque          = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //settings
    const settings        = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]

    //registration
    const register        = [
        {
            name          : "enquiries",
            text          : "please select an item below",
            options       : [
                {
                    label : "Balance",
                    jumpTo : "balance"
                },
                {
                    label : "Mini Statement",
                    jumpTo : "mini-statement"
                },
                {
                    label : "Full Statement",
                    jumpTo : "full-statement"
                }          
            ]
        },
        {
            name          : "phone-number",
            text          : "please select the phone number",
            saveAs        : "credit-account",
            options       : [
                {
                    label : "My Phone",
                    value : 254729347882,
                },
                {
                    label : "Other Phone",
                    jumpTo: 'other-phone'
                }           
            ]
        },
        {
            name          : "other-phone",
            saveAs        : "credit-account",
            text          : "please enter the phone number in the format 07XXXXXXXX"
        },
        {
            name          : "airtime-debit-account",
            saveAs        : "debit-account",
            text          : "please select the account to debit",
            options       : [
                {
                    label : "Account A",
                    value : 12345,
                },
                {
                    label : "Account B",
                    value : 56789
                }           
            ]
        },
        {
            name          : "airtime-amount",
            saveAs        : "amount",
            text          : "please enter the amount to debit"
        },
        {
            name          : "airtime-confirm",
            text          : "Dear @firstname, you are about to purchase @provider airtime worth KES. @amount for @credit-account from @debit-account",
            options       : [
                {
                    label : "Confirm",
                    value : 'airtime-process',
                },
                {
                    label : "Cancel",
                    value : 'app-start'
                }           
            ]
        }
    ]





    /**
     * --------------------------------------
     * 
     *      Custom Module Generator
     * 
     * --------------------------------------
     */
    const moduleGenerator= {

        ['init']                  : ( name, logic  ) => { 

            //generate our dynamic schema
            let schema     = moduleGenerator.schema        ( logic );
            let keys       = moduleGenerator.getAllKeys    ( schema );
            let schemaData = moduleGenerator.analyzeSchema ( keys, schema );
            let template   = moduleGenerator.template      ( schemaData, schema );
            let language   = moduleGenerator.language      ( template ); 
            let config     = moduleGenerator.config        ( schemaData, name );
            let obj        = moduleGenerator.compile       ({ schema, template, language, config }, name );
            return obj;
        },

        //core
        ['schema']                : ( obj ) => {

            let reversed = obj.reverse();

            let schema   = {}

            for ( let index in reversed ) {
                index       = parseInt ( index, 10 )
                let name    = reversed [ index ].name 
                let type    = reversed [ index ].type || false 
                let repeat  =  false 
                let options = reversed [ index ].options || false

                if ( options ) {
                    repeat = reversed [ index ].options.length
                }

                if ( index === 0 ) {
                    schema [ name ]  = {}
                    for ( let i = 0; i < repeat; i ++ ) {
                        schema [ `${name}` ][ `${ options [ i ].value}` ] =  []
                    }
                } 
                else {

                    if ( type !== 'jump') {
                        //clone the compiled object
                        let clone = Object.assign ( {}, schema )

                        if ( repeat ) {

                            //get the object keys of the first item
                            let cloneItemName = Object.keys (clone) [ 0 ]
                            let cloneItem = clone [ cloneItemName ]

                            schema = {}
                            schema [ `${name}` ] =  {}

                            for ( let i = 0; i < repeat; i ++ ) {
                                let jumpTo = options [ i ].jumpTo || false;

                                if ( jumpTo ) {
                                    schema [ `${name}` ][ `${ options [ i ].jumpTo}` ] =  {
                                        [ `${cloneItemName}`] : cloneItem
                                    }
                                }
                                else {
                                    schema [ `${name}` ][ `${cloneItemName}-${ options [ i ].value}` ] =  cloneItem
                                }
                                
                            }
                            schema [ `${name}` ][ 'app-start' ] = []
                        }
                        else {
                            schema  = { 
                                [ `${name}` ] : clone
                            }
                        }
                    }        
                }
            }

            return schema
            
        },
        ['analyzeSchema']         : ( keys, obj ) => {


            let menuData = [];

            for ( let key of keys ) {            
                let data = moduleGenerator.getMenuType ( key, obj );
                menuData.push ( data );
            }

            return menuData;
        },    
        ['template']              : ( obj, schema ) => {

            let template = {};

            let actions = {

                ['list'] : ( entry ) => {

                    var heading =  entry.menu;
                    var items   = entry.children;
                    var itemLabels = [];

                    for ( let item of items ) {
                        itemLabels.push ( item + '-label' );
                    }
                    
                    template [ heading ] = {
                        heading: heading,
                        items : itemLabels
                    };
                },

                ['listOfTypeInput'] : ( entry ) => {

                    var heading =  entry.menu;
                    
                    template [ heading ] = {
                        heading: heading
                    };
                },

                ['input']: ( entry ) => {
                    var heading =  entry.menu;
                    
                    template [ heading ] = {
                        heading: heading
                    };
                },

                ['endpoint']: ( entry ) => {
                    // var heading =  entry.menu;
                    
                    // template [ heading ] = {
                    //     heading: heading
                    // };
                }
            };

            //create the parent template
            let parentKeys = Object.keys ( schema );
            let parentItems = [];
            for ( let item of parentKeys ) {
                parentItems.push ( item + '-label' );
            }
            template [ parentKeys [ 0 ] ] = {
                heading : parentKeys [ 0 ] + '_menu',
                items   : parentItems
            }        

            for ( let entry of obj ) {
                actions [ entry.type ] ( entry );
            }

            return template ;

        },
        ['language']              : ( obj ) => {

            let language = {};
            

            let keys = Object.keys ( obj );

            for ( let key of keys ) {
                

                language [ obj [ key ].heading ] = "";
                

                if ( typeof ( obj [ key ].items ) !== 'undefined' ){

                    let items = obj [ key ].items;

                    for ( let item of items ) {

                        language [ item ] = "";

                        var replaceChars     = item.replace ( /[^0-9]/g, '' );
                        var accountLength    = replaceChars.length;
                        var itemArr          = item.split ( '_' );

                        if ( accountLength > 0 ) {
                            language [ item ] =  itemArr [ itemArr.length - 2 ];
                        } 
                    }
                }



            }

        return language ;    
        },
        ['config']                : ( obj, name ) => {

            let menuMapper    = {};

            //obtain all our inputs
            let actions = {

                ['list'] : ( entry ) => {
                    var heading =  entry.menu;  

                    if ( heading !== "app_start" && heading !== "exit" ){
                        menuMapper [ heading ] = {
                            name           : '',
                            filters        : '',
                            redisHash    : name.charAt(0).toUpperCase() + name.slice ( 1 ),
                            "embeddedInput": false,
                            "embeddedName" : ""

                        };
                    }
                },

                ['input']: ( entry ) => {
                    var heading =  entry.menu;  
                    menuMapper [ heading ] = {
                        name           : '',
                        filters        : '',
                        redisHash      : name.charAt(0).toUpperCase() + name.slice ( 1 ),
                        "embeddedInput": false,
                        "embeddedName" : ""
                    }
                },

                ['endpoint']: ( entry ) => {
                    var heading =  entry.menu;  

                    if ( heading !== "app-start" && heading !== "exit" ){
                        menuMapper [ heading ] = {
                            endpoint:name
                        };
                    }
                },
                
                ['listOfTypeInput']: ( entry ) => {
                    var heading =  entry.menu;  

                    if ( heading !== "app_start" && heading !== "exit" ){
                        menuMapper [ heading ] = {
                            embeddedInput: false,
                            name         : "",
                            redisHash    : name.charAt(0).toUpperCase() + name.slice ( 1 )

                        };
                    }
                },
            };        

            for ( let entry of obj ) {
                actions [ entry.type ] ( entry );
            }

            
            return menuMapper;


        },
        ['compile']               : ( obj, name ) => {

            return {
                schemas      : {
                    [ name ] : obj.schema [ name ]
                },
                templates    : obj.template,
                language     : obj.language,
                config       : {
                    [ name ] : { 
                        menus: obj.config 
                    }
                }
            }

        },

        //Helpers
        [ 'getAllKeys' ]          : ( obj ) => {

            let pendingRecursive = 1;
            let keys             = [];


            //recursive funcion to get all config paths
            let searchObj = ( obj ) => {

                var objectKeys = Object.keys( obj );         

                objectKeys.some( ( k ) => {

                    if ( obj [ k ] && typeof obj [ k ] === 'object') {
                        pendingRecursive++;
                        keys.push ( k  );                
                        searchObj ( obj [ k ] );
                    }
                    
                });
                

                //decrement the recursive flag
                if (--pendingRecursive === 0){
                    return keys.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
                }  

            };
            
            return searchObj ( obj ) ;
        },
        ['getMenuType']           :  ( key, obj ) => {

            try {

                let listOfTypeInput = Object.keys ( obj );

                    
                /**-----------------------------------
                 * recursiveObjectSearch ( menu )    |
                 * -----------------------------------
                 */
                let parent        = moduleGenerator.recursiveObjectSearch ( key, obj );  


                /**
                 * We check if the parent is an object in order to determine if we will treat 
                 * the object as a menu, or an input or an end point
                 */
                if ( typeof( parent ) === "object" ) {
                
                    let parentKeys      = Object.keys ( parent );
                    let parentKeysCount = parentKeys.length;

                    

                    // [ LIST ] Its a menu if the object keys length is greater than 1
                    if ( parentKeysCount > 1 ) {
                        returnObj =  { "valid":true , "menu":key, "type" : "list", children : parentKeys };
                    }

                    // [ INPUT ] its either an input or an end point
                    if ( parentKeysCount === 1 ) {
                        returnObj =  { "valid":true , "menu":key , "type" : "input" };
                    }

                    // [ ENDPOINT ] check to see if its an endpoint
                    let getEndPointChildren = moduleGenerator.recursiveObjectSearch ( key, obj );
                    if ( getEndPointChildren instanceof Array ) { 
                        returnObj =  { "valid":true , "menu":key , "type" : "endpoint" };
                    }
                    
                    // [ LIST OF TYPE INPUT ];
                    // let siblings=  ObjectHelpers.getParents ( obj, key );

                    // if ( listOfTypeInput.includes ( key ) && parentKeys.length === 1  ) {
                    //     returnObj =  { "valid":true , "menu":key, "type" : "listOfTypeInput" };
                    // }
                    // if ( returnObj.type === 'input' && siblings.length > 1 ) {
                    //     returnObj.type = 'listOfTypeInput';
                    // }

                    // if ( key === 'loanApplication_confirm' ){
                    //     console.log ( returnObj,siblings );
                    // }

                    return returnObj;

                    
                }

            }
            catch ( e ) {
                console.log ( e.message );
            }
                


            

            
            
        },
        ['recursiveObjectSearch'] : ( key, obj ) => {

            
            //This is basically all our schema definitions
            let haystack =  [
                obj
            ];

            let searchResult;

            switch ( key ) {
                


                default:
                    var result;

                    for ( var c = 0 ; c  < haystack.length ; c ++ ) {

                        let haystackItem = haystack [ c ];
                        
                        result =  moduleGenerator['searchByKey'] ( haystackItem , key );

                        if ( typeof ( result ) !== "undefined" ) {
                            searchResult = result ;
                        }
                    }
                    
                break;
            }

        return searchResult;

        } ,    
        ['searchByKey']           : ( object, key ) => {

            let findValue = ( object, key ) => {

                //Our objects value will be stored here
                var value;
                var objectKeys = Object.keys( object );

                objectKeys.some( ( k ) => {

                    if (k === key) {
                        value = object[k];
                        return true;
                    }
                    if ( object [ k ] && typeof object [ k ] === 'object') {
                        value = findValue( object [ k ] , key );
                        return value !== undefined;
                    }
                });
                return value;
            }

            var value = findValue ( object, key ) ;
            return value;
            
        }

    }

    let result = moduleGenerator.init  ( 'airtime', airtime )

    console.log ( JSON.stringify ( result, null, 4 ) )
}

HarrysAwesomepatch ()