class Staging {
    constructor (placeholders) {
        this.fs                   = require ( 'fs' );
        this.shell                = require('shelljs');
        this.serviceTemplate      = this.fs.readFileSync ( './templates/service_template.txt','utf-8' );
        this.nginxTemplate        = this.fs.readFileSync ( './templates/nginx_template.txt','utf-8' );
        this.rcLocalTemplate      = this.fs.readFileSync ( './templates/rclocal_template.txt','utf-8' );
        this.phpTemplate          = this.fs.readFileSync ( './templates/template.php','utf-8' );
        this.readmeTemplate       = this.fs.readFileSync ( './templates/readme.md','utf-8' );
        this.placeholders         = placeholders;

        //make directories
        this.folder_path          = `./${this.placeholders [ '{ussd_name}' ]}_config`
        this.phpPath              = `${this.folder_path}/${this.placeholders [ '{ussd_name}' ]}`;
        this.shell.mkdir('-p', this.folder_path);
        this.shell.mkdir('-p', this.phpPath);
    }
    start () {
        this.writeConfigs ()
    }
    writeConfigs () {

        //replace placeholders in the service file
        let placeholderKeys = Object.keys ( this.placeholders );

        for ( let key of placeholderKeys ) {
            if ( key !== '' ) {
                let regex = new RegExp ( key, 'g' )
                if ( key.includes('nginx_port') || key.includes('nodejs_port') ) {
                    this.nginxTemplate = this.nginxTemplate.replace ( regex, this.placeholders [ key ] )
                }
                else {
                    this.serviceTemplate = this.serviceTemplate.replace ( regex, this.placeholders [ key ] )
                }
                
            }
        }

        //write service
        this.fs.writeFileSync ( `${this.folder_path}/${this.placeholders [ '{ussd_name}' ]}_service`, this.serviceTemplate, 'utf-8' )


        //write nginx
        this.fs.writeFileSync ( `${this.folder_path}/${this.placeholders [ '{ussd_name}' ]}.conf`, this.nginxTemplate, 'utf-8' )


        //write rc.local
        let serviceStartCmd = `sh /etc/init.d/${this.placeholders [ '{ussd_name}' ]}_service start\n`
        this.rcLocalTemplate = this.rcLocalTemplate + serviceStartCmd;
        this.fs.writeFileSync ( `${this.folder_path}/rc.local`, this.rcLocalTemplate, 'utf-8' )


        //write php
        this.phpTemplate = this.phpTemplate.replace(/{nodejs_port}/g,this.placeholders [ '{nodejs_port}' ])
                                .replace(/{nodejs_route}/g,this.placeholders [ '{nodejs_route}' ])
                                .replace(/{ussd_name}/g,this.placeholders [ '{ussd_name}' ])


        this.fs.writeFileSync ( `${this.phpPath}/index.php`, this.phpTemplate, 'utf-8' )

        //write readme
        this.readmeTemplate = this.readmeTemplate.replace(/{ussd_name}/g,this.placeholders [ '{ussd_name}' ])
        this.fs.writeFileSync ( `${this.folder_path}/readme.md`, this.readmeTemplate, 'utf-8' )
    }
}
module.exports = Staging;