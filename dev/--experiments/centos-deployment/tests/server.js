const formidable = require ( 'formidable' )
const path       = require ( 'path' )
const fs         = require ( 'fs' )
const express    = require ( 'express' )
const app        = express ( )
const PORT       = 8080

app
.all ( '/', ( req, res ) => {
    res.sendFile ( path.resolve( __dirname, 'public/upload.html') )
})
.all ( '/upload', ( req, res ) => {

    var form = new formidable.IncomingForm();

    form.parse ( req, ( err, fields, files ) => {

        //create the config files in the filesystem
        let placeholders        = {
            '{ussd_name}'         : fields.project_name,
            '{monitoring_service}': fields.mtype, 
            '{entry_file}'        : fields.nodejs_entry_file,
            '{nginx_port}'        : fields.nginx_port,
            '{nodejs_port}'       : fields.nodejs_port,
            '{nodejs_route}'      : fields.nodejs_route
        }
        let Staging = require ( './staging' )
        let staging = new Staging(placeholders);
        staging.start()

        //unzip the project files

        //start the service

        if (err) throw err;

        var oldpath = files.filetoupload.path
        var newpath = path.resolve ( __dirname, 'uploads', files.filetoupload.name )

        fs.renameSync ( oldpath, newpath )
        res.write('File uploaded and moved!')
        res.end()
    });    
})
.listen ( PORT, ( ) => {
    console.log ( 'server listening on port %d', PORT )
});