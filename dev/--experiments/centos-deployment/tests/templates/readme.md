# {ussd_name} Deployment steps

## `1. PHP Configuration:`

    copy `{ussd_name}` folder to `10.20.2.6`, target folder path: `var/www/html-8085/`

## `2. Service Configuration:`

    copy `{ussd_name}_service` file to `10.20.2.24`, target folder path: `/etc/init.d/`

## `3. Nginx Configuration:`

    copy `{ussd_name}.conf` file to `10.20.2.24`, target folder path: `/etc/nginx/conf.d/`

## `4. Service Autorun:`

    copy the last entry of `rc.local` to `10.20.2.24` target file : `/etc/rc.local`