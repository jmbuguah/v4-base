<?php

$redirectTo = "http://10.20.2.24:{nodejs_port}/{nodejs_route}/?MOBILE_NUMBER=" . $_GET["MOBILE_NUMBER"] . "&IMSI=" . $_GET["IMSI"] . "&SERVICE_CODE=" . $_GET["SERVICE_CODE"] . "&SESSION_ID=" . $_GET["SESSION_ID"] . "&USSD_BODY=" . $_GET["USSD_BODY"];


    // Get cURL resource
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $redirectTo,
        CURLOPT_TIMEOUT => 27
    ));

    $resp = curl_exec($curl);
    $httpCode = curl_getinfo($curl)["http_code"];

    // echo $resp, $httpCode ;

    curl_close($curl);

    if (trim($httpCode == "0") || trim($httpCode == "404")) {
        print_r([
            "USSD_BODY" => "Dear customer, {ussd_name} is currently unavailable at the moment. We apologize for the inconvenience. Please try again later.",
            "END_OF_SESSION" => "True"
        ]);
    } 
    else {
        print_r(json_decode($resp, true));
    }
?>