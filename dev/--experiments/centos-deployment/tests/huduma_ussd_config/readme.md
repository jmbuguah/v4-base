# huduma_ussd Deployment steps

## `1.PHP Configuration:`

    copy `huduma_ussd` folder to `10.20.2.6`, target folder path: `var/www/html-8085/`

## `2. Service Configuration:`

    copy `huduma_ussd_service` file to `10.20.2.24`, target folder path: `/etc/init.d/`

## `3. Nginx Configuration:`

    copy `huduma_ussd.conf` file to `10.20.2.24`, target folder path: `/etc/nginx/conf.d/`

## `4. Service Autorun:`

    copy the last entry of `rc.local` to `10.20.2.24` target file : `/etc/rc.local`