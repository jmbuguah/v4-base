"use strict";
/**
 * Redis CRUD class
 * @author: JMbuguah
 * Date: 14-06-2017:1226hrs
 * @description: Class that performs Redis Crud operations
 */
class Store {
    constructor() {
        this.env = require('./env');
        this.connection = {};
        switch (this.env.environment) {
            case 'dev':
                this.connection = this.env.cache.dev;
                break;
            case 'staging':
                this.connection = this.env.cache.staging;
                break;
            case 'prod':
                this.connection = this.env.cache.prod;
                break;
        }
    }
    connect() {
        const redis = require('redis');
        var client = redis.createClient(this.connection.port, this.connection.host);
        client.on('connect', () => {
            // console.log("[Connected] Redis Client on port %d,",this.connection.port," host:",  this.connection.host );
        });
        client.on('end', () => {
            // console.log("[Terminated] Redis Client");
        });
        return client;
    }
    close(client) {
        client.quit();
    }
    async createHash(client, id, data, keyExpiryTime) {
        data = JSON.stringify (data).replace(/null/g,"");
        data = JSON.parse(data);


        let setHash = await new Promise ( ( resolve, reject ) => {
            client.select( this.connection.database , ( err, res ) => {

                if ( err ) reject ( err ); 
                       
                client.hmset( id, data, ( err, res ) => {
                    if( err ) throw err;
                    
                    return resolve(res);
                })
                
                
            });
        })

        if ( id.includes ( 'access_token') &&  keyExpiryTime ) {
            client.expire ( id, keyExpiryTime )
        }
        

        return new Promise ( ( resolve, reject ) => {
            resolve ( setHash )
        })
    }
    readHash(client, id) {
        return new Promise((resolve, reject) => {
            client.select(this.connection.database, (err, res) => {
                if (err) {
                    reject(err);
                }
                client.hgetall(id, (err, data) => {
                    if (err)
                        reject(err);
                    return resolve(data);
                });
            });
        });
    }
    deleteHash(client, id) {
        return new Promise((resolve, reject) => {
            client.select(this.connection.database, (err, res) => {
                if (err)
                    reject(err);
                client.del(id, (err, res) => {
                    if (err)
                        reject(err);
                    resolve(res);
                });
            });
        });
    }
    readMultipleHashes(client, keys) {
        return new Promise((resolve, reject) => {
            client.select(this.connection.database, (err, res) => {
                if (err)
                    reject(err);
                client = client.multi({ pipeline: false });
                keys.forEach((key, index) => {
                    client = client.hgetall(key);
                });
                client.exec((err, res) => {
                    if (err)
                        reject(err);
                    return resolve(res);
                });
            });
        });
    }
    keyExists(client, id) {
        return new Promise((resolve, reject) => {
            client.select(this.connection.database, (err, res) => {
                if (err)
                    reject(err);
                client.exists(id, function (err, reply) {
                    if (reply === 1) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                });
            });
        });
    }
}
//export the Redis Class
module.exports = Store;
