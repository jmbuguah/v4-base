//unsafe , disable rejection of unauthorized ssl certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0

const urls = {
    access       : 'https://10.20.2.24:4445/access-request',
    billers      : 'https://10.20.2.24:4445/assets',
    payment_types: 'https://10.20.2.24:4445/assets',
    airtime      : 'https://10.20.2.24:4445/payments/airtime'
}

class Core  {

    constructor () {

        let fs = require ( 'fs' )
        var rand = require("random-key");

        this.client_id     = 'nnCtF94T_aFbfKmsYgYmd2UyYhnmS13bHMD8ZU_YqtFBddFc'
        this.client_secret = '6QMuqXuzq90Jzfe8UCw9sAxJ4ADzhbPLHmXzDGyFZUDMWoYV'
        this.aes_key       = 'CIHCBIVIeVFcUUGWGXMbTEfIKOEPFGHd'//'CIHCBIVIeVFcUUGWGXMbTEfIKOEPFGHd'
        this.aes_iv        = 'CIHCBIVIeVFcUUGW'//'CIHCBIVIeVFcUUGW'           
        this.postDataAuth  = JSON.stringify({
            'client_id'    : 'nnCtF94T_aFbfKmsYgYmd2UyYhnmS13bHMD8ZU_YqtFBddFc',
            'client_secret': '6QMuqXuzq90Jzfe8UCw9sAxJ4ADzhbPLHmXzDGyFZUDMWoYV',
            'aes_key'      : 'CIHCBIVIeVFcUUGWGXMbTEfIKOEPFGHd',
            'aes_iv'       : 'CIHCBIVIeVFcUUGW'
        })
        this.publicKey     = fs.readFileSync ( 'public.pem' );
        this.Crypto        = require ( "crypto" );
        this.CryptoJS      = require("crypto-js");
        this.currentStatusCode = false;

    }

    async api ( url, data, method ='get') {
        
        const axios         = require ( 'axios' );
        const instance      = axios.create();
        instance.setTimeout = 3000;
        let response        = false;
        let result          = false
        let code            = 0;
        let responseObject = false
        
        try {
            switch  (method) {
                case "get":
                    response = await instance.get ( url, data ).catch ( ( e ) => {

                    })
                    
                break;
    
                case "post":
                    response = await instance.post ( url, data ).catch ( ( e ) => {
                        code =  e.response.status
                        console.log ( { errData : error.response.data } )
                    })
                break;
            }

            if ( response ) {
                code = response.status || false
            
                responseObject =  {
                    code,
                    result:response.data
                }
            }
            else {
                responseObject =  {
                    code,
                    result
                }
            }
    

        }

        catch ( e ) {
            console.log ( e )
        }

        console.log ( { responseObject })

        return responseObject;

    }

    rsaEncrypt ( postDataAuth ) {

        let resultArray =  Buffer.from ( postDataAuth, 'utf8').toLocaleString('base64').match(/(.){1,240}/g);
                     
        let result  = resultArray.map( ( chunk, index ) => {
            return this.Crypto.publicEncrypt({
                key    : this.publicKey,
                padding: this.Crypto.constants.RSA_PKCS1_PADDING
            }, Buffer.from((index +1) + '-' + chunk)).toString('base64');
        })

        return result;

    }

    aesEncrypt ( message, key, iv ) {         

        const { AES, enc } = require ( 'crypto-js' )

        let encrypted =   AES.encrypt ( 
            enc.Utf8.parse ( message ),  
            enc.Utf8.parse( key ), 
            { 
                iv:  enc.Utf8.parse( iv ) 
            } 
        ).toString()

        return encrypted;
        
    }

    aesDecrypt ( message, key, iv ) {
        
        const { AES, enc } = require ( 'crypto-js' )
       
        
        let decrypted =     AES.decrypt ( 
            message, 
            enc.Utf8.parse( key ), 
            { iv: enc.Utf8.parse( iv ) }
        ).toString ( enc.Utf8)
        
        decrypted = decrypted.replace ( /null/g, "\"\"" )

        try {
            decrypted = JSON.parse ( decrypted );
        }
        catch ( e ) {
        }

        return decrypted
    }

    async saveToRedis ( id, data, expiry ) {
        let saveStatus = await this.store.put (id, data, expiry )
        return saveStatus;
    }

    async fetchFromRedis ( id ) {
        let data = await this.store.get (id )
        return data;  
    }
}

class Settings      extends Core {

    constructor () {
        super ()
        this.namespace     = 'huduma@0.0.1'
        this.cache         = require ( './cache/cache' );
        this.store         = new this.cache()
    }

    async fetchAll () {

        console.time    ( 'runCron')        
        //--------------------------

        let access_token   = await this.fetchAccessToken ();
        let billers        = await this.fetchBillers ( access_token );
        let paymentMethods = await this.fetchPaymentMethods ( access_token ) || false;
        

        //save to redis
        if ( billers ) {
            let billers_cache         = [ this.namespace, 'billers'].join ( ':' )
            await this.saveToRedis ( billers_cache, billers, false )
        }

        if ( paymentMethods ) {
            let payment_methods_cache = [ this.namespace, 'payment_methods'].join ( ':' )
            await this.saveToRedis ( payment_methods_cache, paymentMethods, false )
        }
        //---------------------------
        console.timeEnd ( 'runCron')
    }

    async fetchAccessToken () {

        const id  = [ this.namespace, 'access_token' ].join ( ':' )
        let tokenExists = await this.store.exists(id)

        if ( !tokenExists) {
            
            
            let data          = this.rsaEncrypt ( this.postDataAuth )
    
            //run api
            let res = await this.api ( urls.access, data, 'post' )
            let bodyEncrypted = res.result;

            console.log ( 'token deosnt exist', res )
            let result        = this.aesDecrypt ( bodyEncrypted, this.aes_key, this.aes_iv )

            //get token and expiry time
            let token         = result.access_token;
            let expiry        = result.expiry;

            //save token to redis using the expiry time
            let save = await this.saveToRedis ( id, { accesstoken: token }, expiry )
            
            return token;
        }  
        else {
            //fetch token from Redis
            let tokenData = await this.fetchFromRedis ( id );
            return tokenData.accesstoken
        }

    }

    async fetchBillers ( access_token ) {
    
        try {
            let vendors              = [ 'airtime', 'water', 'tv','health','kplc' ]
            let transactionTypesData = []

            for ( let vendor of vendors ) {

                var postBody     = JSON.stringify({   
                    type         : 'transaction-types',
                    vendor       : vendor
                })

                let aesEncrypted     = this.aesEncrypt ( postBody, this.aes_key, this.aes_iv )

                var payload          = JSON.stringify({
                    payload          : aesEncrypted, 
                    access_token     : access_token
                })

                let rsaEncrypted     = this.rsaEncrypt ( payload )

                let res = await this.api ( urls.billers, rsaEncrypted, 'post' )
                let bodyEncrypted = res.result;
                let result           = this.aesDecrypt ( bodyEncrypted, this.aes_key, this.aes_iv )
                transactionTypesData = [ ...transactionTypesData, ... result ]
                
            }

            let transactionTypesObj = {}

            for ( let transaction of transactionTypesData ) {
                transactionTypesObj [ transaction.name.replace ( /\s/g,'_' ).toLowerCase() ] = {
                    name            : transaction.name,
                    id              : transaction.id,      
                    fee             : transaction.fee,       
                    limit           : transaction.limit,       
                    minimum         : transaction.minimum,       
                    amountMultiples : transaction.amountMultiples,     
                    penaltyMin      : transaction.penaltyMin,      
                    penaltyLimit    : transaction.penaltyLimit,      
                    penaltyMultiples: transaction.penaltyMultiples,      
                    hasNoBill       : transaction.hasNoBill,   
                    countyId        : transaction.countyId,      
                    others          : transaction.others
                }
            }

            return transactionTypesObj;
        }
        catch ( e ) {
            console.log ( e )
        }

    }

    async fetchPaymentMethods  ( access_token ) {
    
        try {
            let paymentModes = []

            var postBody     = JSON.stringify({   
                type         : 'payment-modes'
            })

            let aesEncrypted     = this.aesEncrypt ( postBody, this.aes_key, this.aes_iv )

            var payload          = JSON.stringify({
                payload          : aesEncrypted, 
                access_token     : access_token
            })

            let rsaEncrypted     = this.rsaEncrypt ( payload )

            let res = await this.api ( urls.payment_types, rsaEncrypted, 'post' )
            let bodyEncrypted = res.result;
            let result           = this.aesDecrypt ( bodyEncrypted, this.aes_key, this.aes_iv )
            paymentModes = [ ...paymentModes, ... result ]
            
        

            let paymentTypes = {}

            for ( let mode of paymentModes ) {
                paymentTypes [ mode.name.replace ( /\s/g,'_' ).toLowerCase() ] = {
                    name   : mode.name, 
                    id     : mode.id, 
                    fee    : mode.fee,       
                    limit  : mode.limit,      
                    minimum: mode.minimum,
                    group  : mode.group
                }
            }

            return paymentTypes;
        }
        catch ( e ) {
            console.log ( e )
        }        
    }    


}

class Presentments  extends Core {
    constructor ( ) {
        super ()
    }
}

class Api           extends Settings {

    constructor ( ) {
        super ()
    }

    async buyAirtime ( request  ) {

        let  { paymentMethod, biller, amount, credit_account, debit_account } = request;

        try {

            let access_token = await this.fetchAccessToken ();

            //fetch the service and payment method
            let billersId        = [ this.namespace, 'billers' ].join ( ':' );
            let paymentMethodsId = [ this.namespace, 'payment_methods' ].join ( ':' );
            let billers          = await this.fetchFromRedis ( billersId )
            let paymentMethods   = await this.fetchFromRedis ( paymentMethodsId )
            let serviceId        = billers [ biller ].id
            let paymentId        = paymentMethods [ paymentMethod ].id

            var postBody     = JSON.stringify({   
                amount       : parseInt ( amount, 10 ),
                service      : serviceId,
                paymentMethod: paymentId,
                mobile       : credit_account,
                paymentMobile: debit_account
            });

            let aesEncrypted     = this.aesEncrypt ( postBody, this.aes_key, this.aes_iv )

            var payload          = JSON.stringify({
                payload          : aesEncrypted, 
                access_token     : access_token
            })

            let rsaEncrypted     = this.rsaEncrypt ( payload )


            let res = await this.api ( urls.airtime, rsaEncrypted, 'post' )
            let bodyEncrypted = res.result; 
            
            let result           = this.aesDecrypt ( bodyEncrypted, this.aes_key, this.aes_iv )

            if ( res.code === 200 && result === '' ) {
                console.log ( { code : res.code } )
                return true
            }
            else {
                return false
            }

            
        }
        catch ( e ) {
            console.log ( e )
        }     
    }
}




//Api Testing
async function test () {

    console.time ( 'BuyAirtime')
    //----------------------------------------------

    let   a = new Api ()
    let request = {
        paymentMethod : 'mpesa',
        biller        : 'safaricom',
        amount        : '2000',
        credit_account: '254729347882',
        debit_account : '254727519227',
    }
    let buyAirtime = await a.buyAirtime ( request )
    // console.log ( { buyAirtime })

    //----------------------------------------------
    console.timeEnd ( 'BuyAirtime')



    //Testing
    // let s = new Settings ()
    // s.fetchAll ()

}

test ()