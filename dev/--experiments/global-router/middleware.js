
/**
 * getRequest  [ Middleware ] gets the MNO Request and converts it to a standard Eclectics Format
 */
let getRequest =          ( request, response, next ) =>  {

    let queryTemplate = {}

    switch ( request.params.mno ) {
        case 'safaricom':            
            let body = request.query.USSD_STRING.split ( '*' )
            queryTemplate [ 'SESSION_ID']   = request.query.SESSION_ID
            queryTemplate [ 'SERVICE_CODE'] = request.query.SERVICE_CODE
            queryTemplate [ 'MOBILE_NUMBER']= request.query.MSISDN
            queryTemplate [ 'IMSI']         = request.query.IMSI 
            queryTemplate [ 'USSD_BODY']    = body [ body.length -1 ]
        break;

        case 'airtel':
            queryTemplate [ 'SESSION_ID']   = request.query.sessionid
            queryTemplate [ 'SERVICE_CODE'] = request.query.newrequest // 1- new dial
            queryTemplate [ 'MOBILE_NUMBER']= request.query.msisdn
            queryTemplate [ 'IMSI']         = request.query.IMSI 
            queryTemplate [ 'USSD_BODY']    = request.query.input
        break;

    }

    //save as the standard query template
    request.query.UssdQuery = queryTemplate;

    next ();
}

/**
 * getConfig  [ Middleware ] gets the Ussd Config from the mssql Routing Table
 */
let  getConfig =     async( request, response, next )  => {


    /** ----------------------------------
     * 
     *  GET BASE_USSD_CODE AND SUFFIX_CODE
     * 
     * -----------------------------------
     */
    let serviceCode      = request.query.UssdQuery.SERVICE_CODE;
    console.log ( { serviceCode } )
    let serviceCodeParts = serviceCode.split ( '*' ).filter ( ( item ) => {
        return item && typeof item !== 'undefined' && item.trim() !== '' && item !== null
    });
    let base_ussd_code   = `*${ serviceCodeParts [ 0 ] }#`
    let suffix_code      = serviceCodeParts [ serviceCodeParts.length - 1 ]; 




    /** ----------------------------------
     * 
     *  FETCH CONFIG FROM SQL SERVER
     * 
     * -----------------------------------
     */
    let connection = {
        db      : 'ussd_router',
        db_user : 'sa',
        db_pass : '!@#qweASD',
        db_table: 'routing_table'
    }        
    let data = {};
    let url  = '';


    let Sequelize   = require ( 'sequelize' );
    const sequelize = new Sequelize ( connection.db, connection.db_user, connection.db_pass, {
        host: '10.20.2.22',
        dialect: 'mssql',
        dialectOptions: {
            encrypt: false
        },
        operatorsAliases: false,        
        pool: {
            max: 20,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        logging: false            
    });

    const Configurations = sequelize.define( 
        'routing_table',
        {
            PARAM_ID :{
                type: Sequelize.BIGINT,
                primaryKey: true
            },
            PREFERRED_APP_NAME :{
                type: Sequelize.STRING
            },
            APPLICATION_FOLDER_NAME :{
                type: Sequelize.STRING
            },
            PROTOCOL :{
                type: Sequelize.STRING
            },
            DESTINATION_IP :{
                type: Sequelize.STRING
            },
            DESTINATION_PORT :{
                type: Sequelize.STRING
            },
            TARGET_FILE_NAME :{
                type: Sequelize.STRING
            },
            MNO_NAME :{
                type: Sequelize.STRING
            },
            BASE_USSD_CODE :{
                type: Sequelize.STRING
            },
            SUFFIX_USSD_CODE :{
                type: Sequelize.INTEGER
            },
            ACTIVE :{
                type: Sequelize.STRING
            },
            DATE_CREATED :{
                type: Sequelize.STRING
            },
            NODEJS_TARGET :{
                type: Sequelize.STRING
            }
        },            
        {
            // don't add the timestamp attributes (updatedAt, createdAt)
            timestamps: false,
        
            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are enabled
            paranoid: true,
        
            // don't use camelcase for automatically added attributes but underscore style
            // so updatedAt will be updated_at
            underscored: true,
        
            // disable the modification of tablenames; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            freezeTableName: true
        }
    )

    let results =  await Configurations.findAll({
        where: {
            BASE_USSD_CODE  : base_ussd_code,
            SUFFIX_USSD_CODE: suffix_code,
            // MNO_NAME        : request.params.mno.toUpperCase(),
            ACTIVE          : true
        }
    })

    //IMPORTANT: Close our database connection
    sequelize.close();



    /** ----------------------------------
     * 
     *  PROCESS CONFIGURATION INFO
     * 
     * -----------------------------------
     */
    if ( results.length === 1 ) {

        let matchedData   = results [ 0 ].dataValues;
        data.protocol     = matchedData.PROTOCOL;
        data.host         = matchedData.DESTINATION_IP;
        data.port         = matchedData.DESTINATION_PORT;
        data.entryFile    = matchedData.TARGET_FILE_NAME;
        data.path         = matchedData.APPLICATION_FOLDER_NAME;
        data.nodeJsTarget = matchedData.NODEJS_TARGET;

        request.query.appName = data.path;

        //form the URL
        url = `${data.protocol}://${data.host}:${data.port}/${data.path}/`

        if ( data.nodeJsTarget !== 'isTarget') {
            url += `${data.entryFile}`
        }

        //set the url entries
        let querystring = require('querystring');
        url  += `?${ querystring.stringify ( request.query.UssdQuery ) }`;
        
        request.query.hasUrl   = true;
        request.query.protocol = data.protocol;
        request.query.getUrl   = url;
        
        
    }

    else {
        request.query.hasUrl = false;
        request.query.protocol = false;
    }
    
    next ()
}

/**
 * contactUssd [ Middleware ] Attempts a curl request to the target USSD
 */
let  contactUssd =   async( request, response, next ) =>  { 

    console.log ( request.query.appName );

    request.query.responseMsg = {
        'USSD_BODY': `The USSD ${request.query.UssdQuery.SERVICE_CODE} is currently unavailable`,
        'END_OF_SESSION' : 'True'
    };


    if ( typeof request.query.appName !== 'undefined' ) {

        //format the app name
        let appStr = request.query.appName;
        if ( appStr.includes ('/') ) {
            let appStrParts = appStr.split ( '/');
            appStr = appStrParts [ appStrParts.length -1 ]

        }

        
        function camelize(text) {
            return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function(match, p1, p2, offset) {
                if (p2) return p2.toUpperCase();
                return p1.toLowerCase();        
            });
        }

        appStr = appStr.replace( /_/g,' ' ).toUpperCase()


        request.query.responseMsg = {
            'USSD_BODY': `The ${appStr} is currently unavailable`,
            'END_OF_SESSION' : 'True'
        };
    }


    if ( typeof request.query.getUrl === 'undefined' ) {
        request.query.responseMsg = {
            'USSD_BODY': `The Shortcode  \`${request.query.UssdQuery.SERVICE_CODE} \` is not configured on the Eclectics Ussd Servers`,
            'END_OF_SESSION' : 'True'
        };
    }
    
    if ( request.query.hasUrl && typeof request.query.getUrl !== 'undefined'  ) {
        let response  = false;
        try {

            const axios               = require ( 'axios' );
            const instance            = axios.create();
            instance.defaults.timeout = 3000;

            if ( request.query.protocol === 'https' ) {
                response = await instance.post( request.query.getUrl )
                response = response.data
            }
            if ( request.query.protocol === 'http' ) {
                response = await instance.get( request.query.getUrl)
                response = response.data
            }

            //if the response is a strin, get the ussd body and end of session using regular expressions
            if ( typeof response === 'string' ) {

                console.log ( { strResponse: response })

                if ( response.includes ( 'USSD_BODY' ) && response.includes ( 'END_OF_SESSION' ) ) {
                    
                    response = response.replace ( /\n/g,'--br--');
                    let body = response.match (/USSD_BODY(.*)END_OF_SESSION/)[ 1 ].replace (/[\[\]=>]/g,'').trim().replace (/--br--/g,'\n');

                    let session = response.match (/END_OF_SESSION(.*)$/)[ 1 ].replace (/[\[\]=>\)]/g,'').trim().replace (/--br--/g,'\n');

                    session = session.replace ( /\n/g,'').trim()

                    response = {
                        USSD_BODY : body,
                        END_OF_SESSION: session.replace ( /\n/g,'').trim()
                    }
                }
            }
            
        }
        catch ( e ) {
            console.log ( e.message )
        }

        if ( response ) {
            request.query.responseMsg = response;
        }
    } 



    next ()
}

/**
 * sendResponse [ Middleware ] sends the USSD response back to the MNO
 */
let sendResponse =        ( request, response, next ) =>  {

    let message    = request.query.responseMsg [ 'USSD_BODY']
    let endSession = request.query.responseMsg [ 'END_OF_SESSION']

    switch ( request.params.mno ) {
        case 'safaricom':            
            //set safaricom headers
            response.set ({
                'Server'       : 'Eclectics Ussd Server',
                'Content-type' : 'text/html;charset="utf-8"',
                'Accept'       : 'text/html',
                'Cache-Control': 'no-cache',
                'Pragma'       : 'no-cache'
            })

            
            
            let endSessionId = endSession == 'True' ? 'END' : 'CON'

            request.query.responseMsg [ 'USSD_BODY'] = `${endSessionId} ${request.query.responseMsg [ 'USSD_BODY']}`
        break;

        case 'airtel':

            //set airtel headers
            response.set ({
                'Server'        : 'Eclectics Ussd Server',
                'Freeflow'      : endSession,
                'charge'        : 'N',
                'amount'        : '0',
                'Expires'       : '-1',
                'Pragma'        : 'no-cache',
                'Cache-Control' : 'max-age=0',
                'Content-Type'  : 'text/html;charset="utf-8"',
                'Content-Length': message.length
            })
        break;

    }
    next ()
}


module.exports = {
    getRequest,
    getConfig,
    contactUssd,
    sendResponse
}