//unsafe , disable rejection of unauthorized ssl certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0

//import our middleware
const { getRequest, getConfig,contactUssd, sendResponse } = require ( './middleware' )

//set some variables
let app         = require ( 'express' ) ().disable ( 'x-powered-by' );
let ROUTER_PORT = 1994;


//run our server
app.all ( '/:mno', [ getRequest, getConfig, contactUssd, sendResponse ], ( request, response ) => {
    response.send ( request.query.responseMsg [ 'USSD_BODY'] )
})
.listen ( ROUTER_PORT, ( ) => {
    console.log ( 'the app is listening on port: %d', ROUTER_PORT )
})