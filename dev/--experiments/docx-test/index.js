var JSZip         = require('jszip');
var Docxtemplater = require('docxtemplater');
var fs            = require('fs');
var path          = require('path');

//Load the docx file as a binary
var content = fs.readFileSync(path.resolve(__dirname, 'input.docx'), 'binary');
var zip     = new JSZip(content);
var doc     = new Docxtemplater();
doc.loadZip(zip);

//set the templateVariables
doc.setData({
    first_name : 'Everlyn',
    last_name  : 'Mbula',
    phone      : '1234567890',
    description: 'Documentation Example'
});

try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render()
    var buf = doc.getZip().generate({type: 'nodebuffer'});
    fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
}
catch (error) {
    var e = {
        message   : error.message,
        name      : error.name,
        stack     : error.stack,
        properties: error.properties
    }
    console.log(JSON.stringify(e,null,4));
    throw error;
}

