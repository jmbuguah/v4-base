var newman = require('newman'); // require newman in your project
let results = []

// call newman.run to pass `options` object and wait for callback
newman.run({
    collection: require('./collections/testing-shortcodes.json'),
    reporters : [ "cli", "html" ] 
}, function (err) {
	if (err) { throw err; }
    console.log('collection run complete!');
})
.on('request', function (err, args) {
    if (!err) {
        // here, args.response represents the entire response object
        var rawBody = args.response.stream, // this is a buffer
            body = rawBody.toString(); // stringified JSON

            
        let obj = {
            serviceCode: args.request.url.query.members [ 1 ].value,
            response: body,
            available: true
        } 

        if ( body.includes ( 'is not configured') || body.includes ( 'currently unavailable' ) ) {
                obj.available = false;
        }

        results.push(obj); // this is just to aggregate all responses into one object
    }
})
// a second argument is also passed to this handler, if more details are needed.
.on('done', function (err, summary) {
    let fs = require ( 'fs')
    // write the details to any file of your choice. The format may vary depending on your use case
    fs.writeFileSync('migration-report.json', JSON.stringify(results, null, 4));
});