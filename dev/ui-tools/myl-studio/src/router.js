import Vue from 'vue'
import Router from 'vue-router'
import Generator from './views/Generator.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Generator
    },
    {
      path: '/generator',
      name: 'generator',
      component: Generator
    },
    {
      path: '/preview',
      name: 'preview',
      component: () => import( './views/Preview.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Stepper.vue')
    }
  ]
})
