//Vue libs
import Vue      from 'vue'
import App      from './App.vue'
import router   from './router'
import store    from './store'
import Vuetify  from 'vuetify'

//Global CSS
import "./assets/css/vendor/bootstrap.css";
import "./assets/css/vendor/font-awesome.css";
import "./assets/css/vendor/material-design-iconic-font.css";
import "./assets/css/vendor/icomoon.css";
import "./assets/css/vendor/simple-line-icons.css";
import "./assets/css/custom/controller.less";
import 'vuetify/dist/vuetify.min.css' 
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '../node_modules/roboto-fontface/css/roboto/roboto-fontface.css'

// Helpers
// import colors from 'vuetify/es5/util/colors'

//Vue Instance
Vue.use(Vuetify, {
  iconfont: 'mdi',
  theme: {
    primary  : '#4ebcda'//colors.blue.accent1, // #E53935
    // secondary: colors.red.lighten4, // #FFCDD2
    // accent   : colors.indigo.base // #3F51B5
  }
 })
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
