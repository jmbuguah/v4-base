module.exports =  () => {

    return { 
        formatToFloatString : ( input ) => {
            return parseFloat ( input ) .toFixed ( 2 ).toString ()
        },
        formatToRoute       : ( input ) => {
            return `${input.toString ()}`
        },
        formatPageName       : ( input ) => {
            return `${input.replace ( /\s/g,'-').replace (/[_]/g,'').toLowerCase()}-page`
        },
        snakeCase           : ( input ) => {
            return input.replace ( /\s/g,'-').replace (/[_]/g,'').toLowerCase()
        },
        toNumber           : ( input ) => {
            return parseInt ( input, 10 )
        }
    }
}