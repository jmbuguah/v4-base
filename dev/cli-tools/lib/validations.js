module.exports = () => {

    return { 
        isNumber           : ( input ) => {

            let num =  parseInt ( input, 10 )
            let result = `The input \`${input}\` is not a valid number. Expecting a number`

            if ( !isNaN( num ) ) {
                result =  true
            }

            return result;
        },

        isNotEmpty           : ( input ) => {
            let result = `The input \`${input}\` cannot be empty`

            if ( input.trim !== '' ) {
                result =  true
            }

            return result;
        }
    }
}
