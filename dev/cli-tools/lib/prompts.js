
const filters = require ( './filters' )
const validations = require ( './validations' )
const inquirer = require ( 'inquirer')

module.exports                 = {
    newApp                     : [
        {
            "type"             : "list",
            "name"             : "app-name",
            "message"          : "what would you like to do?",
            "choices"          : ["create a new ussd app", 'load an existing app']
        },
        {
            "type"             : "input",
            "name"             : "app-name",
            "message"          : "please enter a name for your ussd app?",
            "filter"           : filters().snakeCase
        },
        {
            "type"             : "input",
            "name"             : "app-version",
            "message"          : "what is the base version  ?",
        },
        {
            "type"             : "list",
            "name"             : "authenticate",
            "message"          : "would you like to enable authentication ?",
            "choices"          : [ "true", "false" ]
        },
        {
            "type"             : "checkbox",
            "name"             : "authenticate",
            "message"          : "Select the defaults to be added?",
            "choices"          : [ "Authentication", "Registration", "Settings" ]
        },
        {
            "type"             : "list",
            "name"             : "authenticate",
            "message"          : "would you like to enable fetching of charges ?",
            "choices"          : [ "true", "false" ]
        }
        
    ],
    menuTypeQuestions          : [
        {
            "type"             : "list",
            "name"             : "menu-type",
            "message"          : "What would you like to do",
            "choices"          : [ 
                {
                    "name"     : "Create a Page",
                    "value"    : "page"
                },
                new inquirer.Separator('----------'),
                {
                    "name"     : "Create a set of Menus",
                    "value"    : "prompt-set",
                    "disabled" : true
                },
                {
                    "name"     : "Create a Standalone Menu",
                    "value"    : "standalone-prompt",
                    "disabled" : true
                }
            ]
            
        }
    ],
    
    //create page
    pageQuestions              : [
        {
            "type"             : "input",
            "name"             : "page-name",
            "message"          : "What would you like to name your page ( e.g client ) ",
            "filter"           : filters ().formatPageName
        },
        {
            "type"             : "list",
            "name"             : "is-root",
            "message"          : "Would you like this page to be a root page",
            "choices"          : [ 
                {
                    "name"     : "No",
                    "value"    : false
                },
                {
                    "name"     : "yes",
                    "value"    : true
                }
            ],
            "default"          : "No"
        }
    ],
    pageOptionsNumberQuestions : [                    
        {
            "type"             : "input",
            "name"             : "options-number",
            "message"          : "A Page is a select menu containing options that link to other pages, sets of menus or standalone menus. How many options does the page have ?",
            "validate"         : validations().isNumber
            
        }
    ],
    pageOptionsDetailsQuestions: [                    
        {
            "type"             : "input",
            "name"             : "option-name",
            "message"          : "What would you like to name your option",
            "default"          : "",
            "filter"           : filters ().snakeCase,
            "validate"         : validations().isNotEmpty

        },

        {
            "type"             : "list",
            "name"             : "option-link-type",
            "message"          : "What type of Menu does the option link to ?",
            "choices"          : [ 
                {
                    "name"     : "A set of Menus",
                    "value"    : "option-prompt-set"
                },
                {
                    "name"     : "A Page",
                    "value"    : "option-page"
                },
                {
                    "name"     : "A Standalone Menu",
                    "value"    : "option-standalone-prompt"
                }
            ],
            "default"          : "option-prompt-set"
        },
        {
            "type"             : "list",
            "name"             : "option-authenticate",
            "message"          : "Will the option be password protected ?",
            "choices"          : [
                {
                    "name"     : "No",
                    "value"    : false
                },
                {
                    "name"     : "Yes",
                    "value"    : true
                }                            
            ],
            "default"          : "No"
        },
        {
            "type"             : "list",
            "name"             : "option-enabled",
            "message"          : "Is the option enabled by default ?",
            "choices"          : [ 
                {
                    "name"     : "Yes",
                    "value"    : true
                },
                {
                    "name"     : "No",
                    "value"    : false
                }
            ],
            "default"          : "Yes"
        },
    ]
}