

/*----------------------------
| USSD COMMANDER DEPS          |
|-----------------------------*/
/**
 * 
 */
var Chalk       = require('chalk');
var Clear       = require('clear');
var CLI         = require('clui');
var commander   = require ( 'commander' );
var Figlet      = require('figlet');
var Inquirer    = require('inquirer');
var Preferences = require('preferences');
var Spinner     = CLI.Spinner;
var Lodash      = require('lodash');
var Touch       = require('touch');
var basePath    = require ( 'path' ).resolve ( __dirname, "..", "..", "dist","www")
let prompts     = require ( './lib/prompts' )


module.exports  = {
    Chalk,
    Clear,
    CLI,
    commander,
    Figlet,
    Inquirer,
    Preferences,
    Spinner,
    Lodash,
    Touch,
    basePath,
    prompts
}