
class CliTools {

    constructor () {

        this.init     = require ( "./autoLoad.js" )
        this.path     = require ( 'path')
        this.program  = require ( 'commander')
        this.inquirer = require ( 'inquirer')        
        this.fs       = require ( 'fs' );
        this.basePath = this.init.basePath
        this.created  = '';
        
        this.showHeader ()
        
    }

    async start             () {
        /**
         * Type of select:
         * - page
         * - default
         * - transact
         * - navigate
         * - update-parameters
         * - update-local
         */

        this.showActionLabel ( 'ussd-cli@4.0.0', this.init.Chalk.green ( this.created ) ) 
        this.created = '';

        let menuTypeAnswers = await this.inquire ( this.init.prompts.menuTypeQuestions );

        switch ( menuTypeAnswers [ 'menu-type' ] )  {
            case "page" : 
               await this.createPage ()
            break;

            case "standalone-prompt":
            break;

            case "prompt-set":
            break;
        }

        

        
        //recursive call back to start
        this.start ()



        
    }

    async testingEnquirer () {
        const { prompt } = require ( 'enquirer')
        // const response = await prompt (
        //     this.init.prompts.menuTypeQuestions
        // )

    }

    async createPage        () {

        
        
        let pageTemplate          = {
            "type"                : "select",
            "name"                : "",
            "previous"            : "",
            "options"             : [],
            "error"               : ""
        
        };
        let pageObject            = {}
        let template              = Object.assign ( {}, pageTemplate );
        
        /**
         * HANDLE PAGE DATA
         */
        let pageAnswers   = await this.inquire ( this.init.prompts.pageQuestions );                
        template.name     = pageAnswers ['page-name']
        template.previous = '';
        template.error    = `${pageAnswers ['page-name']}-error`;

        //handle if a page is the root page or not
        if ( pageAnswers [ 'is-root'] ) delete template.previous;
        pageObject = template

        /**
         * HANDLE PAGE OPTIONS DATA
         */
        this.showActionLabel ( 'ussd-cli@4.0.0: Page Options' )  
        let pageOptionsNumberAnswers = await this.inquire ( this.init.prompts.pageOptionsNumberQuestions );
        let optionsNum = pageOptionsNumberAnswers [ 'options-number' ];

        //Handle prompts for option details
        this.showActionLabel ( 'ussd-cli@4.0.0: Page Option Details' )       

        for ( let i = 0; i < optionsNum; i ++ ) {

            this.showMinorLabel ( 'ussd-cli@4.0.0 : Option ' + ( i + 1 ) + ' ]'  );

            //get our user defined page options details
            let pageOptionsDetails   = await this.inquire ( this.init.prompts.pageOptionsDetailsQuestions );

            //process the data and if need be create necessary files on disk then push to our page object
            let optionsObject = {
                name        : pageOptionsDetails [ 'option-name' ],
                label       : `${pageOptionsDetails [ 'option-name' ] }-label`,
                enabled     : pageOptionsDetails [ 'option-enabled' ],
                authenticate: pageOptionsDetails [ 'option-authenticate' ]
            }

            //create new files based on the "option-link-type"
            let linkType =  pageOptionsDetails [ "option-link-type" ]

            let name = '';
            let data = '';
            let path = ''
            

            switch ( linkType ) {
                case "option-prompt-set":
                    name = `${optionsObject.name}.json`;
                    data = JSON.stringify ( [], null, 4 );
                    path = `./out/prompts/${name}`
                    this.fs.writeFileSync ( path, data, 'utf-8' )                    
                break;

                case "option-page":
                    name = `${optionsObject.name}-page.json`;
                    data = JSON.stringify ( pageTemplate, null, 4 );                    
                    path = `./out/pages/${name}`
                    this.fs.writeFileSync ( path, data, 'utf-8' )
                break;

                case "option-standalone-prompt":
                    name = `${optionsObject.name}.json`;
                    data = JSON.stringify ( {}, null, 4 );
                    path = `./out/prompts/${name}`
                    this.fs.writeFileSync ( path, data, 'utf-8' )
                break;
            }

            

            pageObject.options.push ( optionsObject )
            
        }

        //create the page
        let name = `${pageObject.name}.json`;
        let data = JSON.stringify ( pageObject, null, 4 );
        let path = `./out/pages/${name}`

        this.fs.writeFileSync ( path, data, 'utf-8' )

        
    }

    async createPromptSet   () {
    }

    async createPrompt      () {
    }


    /**
     * [ HELPER FUNCTIONS ]
     */
    inquire                 ( questions ){
    
        return new Promise ( ( resolve, reject ) => {
            this.inquirer
            .prompt( questions )
            .then(answers => {
                resolve ( answers );
            });
        })
    
    }

    showHeader              () {
        this.init.Clear()
        let ussdText =  (                            
            this.init.Figlet.textSync(
                `ussd`, 
                "isometric3"
            )
        );


        console.log ( '\n\n',ussdText);
        console.log ( `\n  Eclectics International ltd ,. All rights reserved. \n  v4 USSD Engine Cli Tool (c) 2018-2019`)
        console.log ( `  _______________________________________________________\n`);

   
        

    }

    showActionLabel         ( label ) {
        this.showHeader ()
        console.log ( this.init.Chalk.bgCyan ( ' ' ), this.init.Chalk.green ( label ), "\n" ) ;
    }

    showMinorLabel          ( label ) {
        this.showHeader ()
        console.log ( this.init.Chalk.green ( label ) )
    }    
}


let test = async () => {
    let ct = new CliTools () ;
    ct.start() ;
    // ct.testingEnquirer()
}

test ();
