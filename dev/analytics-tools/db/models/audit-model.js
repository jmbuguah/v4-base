const mongoose = require ( 'mongoose' )

const auditSchema = mongoose.Schema ( {
    'timestamp'   : { type: String },
    'input'       : { type: String },
    'type'        : { type: String },
    'menuAccessed': { type: String },
    'userId'      : { type: String },
    'ussdService' : { type: String }
})



module.exports = mongoose.model ( 'audit', auditSchema )