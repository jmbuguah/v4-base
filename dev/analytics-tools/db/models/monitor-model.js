const mongoose = require ( 'mongoose' )

const monitorSchema = mongoose.Schema ( {
    'timestamp': { type: String },
    'app'      : { type: String },
    'code'     : { type: String },    
    'available': { type: Boolean },
    'response' : { type: String }
})


module.exports = mongoose.model ( 'monitor', monitorSchema )