const mongoose = require ( 'mongoose' )

const transactionSchema = mongoose.Schema ( {
    'timestamp'        : { type: String },
    'transactionName'  : { type: String },
    'type'             : { type: String },
    'charge'           : { type: String },
    'transactionAmount': { type: String },
    'transactionId'    : { type: String },
    'userId'           : { type: String },
    'ussdService'      : { type: String }
})

module.exports = mongoose.model ( 'transaction', transactionSchema )