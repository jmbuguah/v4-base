
const mongoose = require ( 'mongoose' );

//bind mongoose promises to global promise
mongoose.Promise = global.Promise

//connect to a db
const db = 'ussd-analytics'

mongoose.connect ( `mongodb://localhost:27017/${db}`,{ 
    useNewUrlParser: true 
})

//import model
const Audit = require ( '../models/audit-model' )


//add transaction
const addAudit = ( audit ) => {

    return new Promise ( ( resolve, reject ) => {
        Audit
        .create ( audit )
        .then ( tx => {
            console.info ( 'new Audit added' )
            // mongoose.disconnect ()
            resolve ( 'The Audit was added successfully' )
        })
        .catch ( ( e ) => {
           reject ( e.message )
        })
    })

}

//Export
module.exports = { 
    addAudit
}