
const mongoose = require ( 'mongoose' );

//bind mongoose promises to global promise
mongoose.Promise = global.Promise

//connect to a db
const db = 'ussd-analytics'

mongoose.connect ( `mongodb://localhost:27017/${db}`,{ 
    useNewUrlParser: true 
})

//import model
const Transaction = require ( '../models/transaction-model' )


//add transaction
const addTransaction = ( transaction ) => {

    return new Promise ( ( resolve, reject ) => {
        Transaction
        .create ( transaction )
        .then ( tx => {
            console.info ( 'new Transaction added' )
            // mongoose.disconnect ()
            resolve ( 'The Transaction was added successfully' )
        })
        .catch ( ( e ) => {
           reject ( e.message )
        })
    })

}

//find transaction
const findTransaction = ( account ) => {

    Transaction
        .find ( account )
        .then ( tx => {
            console.info ( tx )
            console.info ( `${ tx.length } matches found` )
            mongoose.disconnect ()
        })
        .catch ( ( e ) => {
            console.log ( e.message )
        })
}

//Export
module.exports = { 
    addTransaction,
    findTransaction
}