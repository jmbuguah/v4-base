
const program = require ( 'commander' );
const { prompt } = require ( 'inquirer' )

const { 
    addTransaction, 
    findTransaction 
} = require ( './models/transaction-controller' );


let formatToFloatString = ( input ) => {
    return parseFloat ( input ) .toFixed ( 2 ).toString ()
}

let formatToRoute = ( input ) => {
    return `/${input.toString ()}`
}


const questions = [
    {
        "type" :"input",
        "name" : "customerAccount",
        "message" : "Enter the Customers account"
    },
    {
        "type" :"input",
        "name" : "amount",
        "message" : "Enter the transacted amount",
        "filter" : formatToFloatString
    },
    {
        "type" :"input",
        "name" : "charge",
        "message" : "Enter the charge",
        "filter" : formatToFloatString
    },
    {
        "type" :"input",
        "name" : "apiRoute",
        "message" : "Enter the transaction api route",
        "filter" : formatToRoute
    }
]


program
    .version ( '1.0.0')
    .description ( 'Analytics system')

program
    .command ( 'add')
    .alias ( 'a')
    .description ( 'Add an analytics Api transaction entry')
    .action (  () => {
        prompt ( questions ). then ( answers  => addTransaction ( answers) )
    })

program
    .command ( 'find <customerAccount>')
    .alias ( 'f')
    .description ( 'Find an analytics Api transaction entry')
    .action (  ( customerAccount ) => {
        findTransaction ({ 
            customerAccount
        })
    })

program.parse ( process.argv )