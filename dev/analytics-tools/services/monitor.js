//unsafe , disable rejection of unauthorized ssl certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0

class UssdMonitor {
    /**
     * STEPS:
     * 1. get all configured apps from Mongo DB
     * 2. create a newman test configuration fo all of them
     * 3. run the tests
     * 4. save the reports to mongo or any other transport
     */
    async run               ( ) {
        try {
            console.time ( 'Ussd Monitor run' )
            var Spinner = require('cli-spinner').Spinner;
            var spinner = new Spinner( 'Running the Ussd Monitor...' );
            
            //start a cli spinner
            spinner.setSpinnerString(0);
            spinner.start();

            //fetch configs
            let configs    = await this.getConfigs  (  )

            //scaffold a newman test
            let newmanTest = this.createTest        ( configs )

            //run the newman test
            console.time ( 'newman Test Run')
            let testResult = await this.runTest     ( newmanTest )
            console.timeEnd ( 'newman Test Run')

            //stop the cli spinner
            spinner.stop();

            //save the data for analytics purposes
            let saved =await this.saveData ( testResult )
            console.timeEnd ( 'Ussd Monitor run' )
        }
        catch ( e ) {
            console.log ( e.message )
        }
         
    }

    //STEP 1
    async  getConfigs       ( ) {

        /** ----------------------------------
         * 
         *  FETCH CONFIG FROM SQL SERVER
         * 
         * -----------------------------------
         */
        let connection = {
            db      : 'ussd_router',
            db_user : 'sa',
            db_pass : '!@#qweASD',
            db_table: 'routing_table'
        }        

        let allData = [];

        let Sequelize   = require ( 'sequelize' );
        const sequelize = new Sequelize ( connection.db, connection.db_user, connection.db_pass, {
            host: '10.20.2.22',
            dialect: 'mssql',
            dialectOptions: {
                encrypt: false
            },
            operatorsAliases: false,        
            pool: {
                max: 20,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            logging: false            
        });

        const Configurations = sequelize.define( 
            'routing_table',
            {
                PARAM_ID :{
                    type: Sequelize.BIGINT,
                    primaryKey: true
                },
                PREFERRED_APP_NAME :{
                    type: Sequelize.STRING
                },
                APPLICATION_FOLDER_NAME :{
                    type: Sequelize.STRING
                },
                PROTOCOL :{
                    type: Sequelize.STRING
                },
                DESTINATION_IP :{
                    type: Sequelize.STRING
                },
                DESTINATION_PORT :{
                    type: Sequelize.STRING
                },
                TARGET_FILE_NAME :{
                    type: Sequelize.STRING
                },
                MNO_NAME :{
                    type: Sequelize.STRING
                },
                BASE_USSD_CODE :{
                    type: Sequelize.STRING
                },
                SUFFIX_USSD_CODE :{
                    type: Sequelize.INTEGER
                },
                ACTIVE :{
                    type: Sequelize.STRING
                },
                DATE_CREATED :{
                    type: Sequelize.STRING
                },
                NODEJS_TARGET :{
                    type: Sequelize.STRING
                }
            },            
            {
                // don't add the timestamp attributes (updatedAt, createdAt)
                timestamps: false,
            
                // don't delete database entries but set the newly added attribute deletedAt
                // to the current date (when deletion was done). paranoid will only work if
                // timestamps are enabled
                paranoid: true,
            
                // don't use camelcase for automatically added attributes but underscore style
                // so updatedAt will be updated_at
                underscored: true,
            
                // disable the modification of tablenames; By default, sequelize will automatically
                // transform all passed model names (first parameter of define) into plural.
                // if you don't want that, set the following
                freezeTableName: true
            }
        )

        let results =  await Configurations.findAll({
            where: {
                ACTIVE  : true,
                MNO_NAME: 'SAFARICOM'
            }
        })

        //IMPORTANT: Close our database connection
        sequelize.close();




        /** ----------------------------------
         * 
         *  PROCESS CONFIGURATION INFO
         * 
         * -----------------------------------
         */
        if ( results.length > 0 ) {

            for ( let index in results ) {

                let matchedData   = results [ index ].dataValues;
                let data = {};
                data.protocol     = matchedData.PROTOCOL;
                data.host         = matchedData.DESTINATION_IP;
                data.port         = matchedData.DESTINATION_PORT;
                data.entryFile    = matchedData.TARGET_FILE_NAME;
                data.path         = matchedData.APPLICATION_FOLDER_NAME;
                data.nodeJsTarget = matchedData.NODEJS_TARGET;
                data.appName      = data.path;

                //code
                let baseCode = matchedData.BASE_USSD_CODE.replace ( /#/g,'*')
                let suffix_ussd_code = matchedData.SUFFIX_USSD_CODE;

                //form the URL
                let url = `${data.protocol}://${data.host}:${data.port}/${data.path}/`

                if ( data.nodeJsTarget !== 'isTarget') {
                    url += `${data.entryFile}`
                }

                /**
                 * Dummy Query params
                 */
                let QueryParams = {
                    SERVICE_CODE:`${baseCode}${suffix_ussd_code}`,
                    USSD_BODY:'',
                    MOBILE_NUMBER:'254729347882',
                    IMSI:'25434565439',
                    SESSION_ID: '098124781'
                }

                //set the url entries
                let querystring = require('querystring');
                url  += `?${ querystring.stringify ( QueryParams ) }`;
                data.url = url;
                data.serviceCode = `${baseCode}${suffix_ussd_code}`;
                allData.push ( data );
            }
            
        }
        // console.log ( { allData } )
        return allData;
    }

    //step2
    createTest              ( configs ) {

        let testTemplate = {
            "info": {
                "_postman_id": "1b652823-a77f-4fcb-b797-214f42245788",
                "name": "ussd-monitor",
                "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
            },
            "item": []
        }

        for ( let config of configs ) {

            let method = 'POST';
            if ( config.protocol === 'http' ) {
                method = 'GET'
            }

            let path = `${config.path}/${config.entryFile}`
            if ( config.nodeJsTarget === 'isTarget') {
                path = `${config.path}/`
            }
            
            let individualTemplate = {
                "name": config.appName,
                "event": [
                    {
                        "listen": "test",
                        "script": {
                            "id": "4f5303c0-f0b9-43b6-bda5-f1ed52dbd828",
                            "exec": [
                                "console.log( { status:responseCode.code , msg: responseBody } )",
                                ""
                            ],
                            "type": "text/javascript"
                        }
                    }
                ],
                "request": {
                    "method": method,
                    "header": [],
                    "body": {
                        "mode": "raw",
                        "raw": ""
                    },
                    "url": {
                        "raw": config.url,
                        "protocol": config.protocol,
                        "host": [
                            config.host
                        ],
                        "port": config.port,
                        "path": [
                            path
                            
                        ],
                        "query": [
                            {
                                "key": "SESSION_ID",
                                "value": "098124781"
                            },
                            {
                                "key": "SERVICE_CODE",
                                "value": config.serviceCode
                            },
                            {
                                "key": "MOBILE_NUMBER",
                                "value": "254729347882"
                            },
                            {
                                "key": "IMSI",
                                "value": "25434565439"
                            },
                            {
                                "key": "USSD_BODY",
                                "value": ""
                            }
                        ]
                    }
                },
                "response": []
            }       
            testTemplate.item.push ( individualTemplate );
        }
        
        return testTemplate;
    }

    //step3
    runTest                 ( testParams ) {

        return new Promise ( ( resolve, reject ) => {

            var newman  = require('newman'); // require newman in your project
            let results = []

            // call newman.run to pass `options` object and wait for callback
            newman.run  ({
                    collection    : testParams,
                    timeoutRequest: 10000,
                    // reporters  : [ "cli", "html" ] 
                }, 
                ( err ) => {
                    if (err) console.log( err )
                    console.log('Test complete!');
                }
            )
            .on ( 'request' , ( err, args ) => {

                let obj = {
                    app: args.request.url.path [ 0 ],
                    code: args.request.url.query.members [ 1 ].value,
                    response: '',
                    available: false
                } 

                if ( !err ) {
                    // here, args.response represents the entire response object
                    var rawBody = args.response.stream, // this is a buffer
                        body    = rawBody.toString(); // stringified JSON 

                    if ( 
                        !body.includes ( 'USSD_BODY') && 
                        !body.includes ( 'END_OF_SESSION' ) 
                    ){
                        obj.available  = false;
                        const Entities = require('html-entities').AllHtmlEntities; 
                        const entities = new Entities();                    
                        obj.response = entities.encode( body );
                    }
                    else {
                                
                        body = body.replace ( /\n/g,'--br--');
                        let newBody = body.match (/USSD_BODY(.*)END_OF_SESSION/)[ 1 ].replace (/[\[\]=>]/g,'').trim().replace (/--br--/g,'\n');

                        obj.response = newBody;
                        obj.available = true;                                            
                    }

                    
                }

                results.push(obj);
            })

            .on('done', function (err, summary) {
                resolve ( results )
            });

        })
    }

    //step4
    async saveData          ( reports ) {

        const { addMonitorReport } = require ( '../db/controllers/monitor-controller' );           
        const moment               = require ( 'moment' ) 
        const timestamp            = `${moment().unix()}`

        for ( let report of reports ) {
            let reportTemplate = {
                'timestamp': timestamp,
                'app'      : report.app,
                'code'     : report.code,
                'available': report.available,
                'response' : report.response               
            }
    
            try {
                let result = await addMonitorReport ( reportTemplate )
                console.log ( 'report added' )
                console.log ( { report: reportTemplate }  )
            }
            catch ( e ) {
                console.log ( e.message )
            }
        }
                

    }

}


class MonitorCron {
    /**
        *     *    *    *    *    *
        ┬    ┬    ┬    ┬    ┬    ┬
        │    │    │    │    │    │
        │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
        │    │    │    │    └───── month (1 - 12)
        │    │    │    └────────── day of month (1 - 31)
        │    │    └─────────────── hour (0 - 23)
        │    └──────────────────── minute (0 - 59)
        └───────────────────────── second (0 - 59, OPTIONAL)

     */
    
    runSchedule ( ) {

        // this.run()

        var schedule = require('node-schedule');

        var j = schedule.scheduleJob('*/600 * * * * *', (  ) => {
            let um = new UssdMonitor()
            um.run();
        });
    }
}

// let cron = new MonitorCron();
// cron.runSchedule();

let um = new UssdMonitor()
um.run();
