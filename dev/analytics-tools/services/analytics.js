class AnalyticsManager { 

    /**
     *  Add `Built in Analytics` in order to understand the most popular menus by :
        ----------------------------------------------------------------------------
        ☐ component access | app access ( on the menu handler ),
        ☐ revenue & transaction amounts ( on the Api ),
        ☐ transaction ( component ) performance ( whether tx failed or worked ) ( on the http server ),
        ☐ audit trail, for each user action @started(18-10-16 15:22)
     */
    constructor ( ) {
        this.Queue      = require('bull');
        this.queues      = {
            analytics: new this.Queue(  "analytics_queue" )
        }

        this.startQueues () 
    }

    startQueues     ( ) {

        let allQueues = Object.keys ( this.queues );

        for ( let q of allQueues ) {
            console.log ( `starting the ${q} queue..`)
            this.queues [ q ].process( this [ q ])
        }
        
        
    }

    async analytics   (  job, done  ) {

           

        switch ( job.data.type ) {

            case 'transaction':

                const { addTransaction } = require ( '../db/controllers/transaction-controller' );            
            
                let transactionTemplate = {
                    'timestamp'        : job.data.timestamp,
                    'transactionName'  : job.data.name,
                    'type'             : job.data.type,
                    'charge'           : job.data.charge,
                    'transactionAmount': job.data.amount,
                    'transactionId'    : job.data.txid,
                    'userId'           : job.data.userid,
                    'ussdService'      : job.data.ussdservice
                }

                try {
                    let result = await addTransaction ( transactionTemplate )
                    console.log (
                        "Transaction done", 
                        { 
                            result 
                        }
                    );
                    
                    job.progress ( 100 );
                    done();
                }

                catch ( e ) {
                    console.log ( e.message )
                }
                
            break;

            case 'audit':

                const { addAudit } = require ( '../db/controllers/audit-controller' );            

                
                let auditTemplate = {
                    'timestamp'   : job.data.timestamp,
                    'input'       : job.data.input,
                    'type'        : job.data.type,
                    'menuAccessed': job.data.menuAccessed,
                    'userId'      : job.data.userid,
                    'ussdService' : job.data.ussdservice
                }

                try {
                    let result = await addAudit ( auditTemplate )
                    console.log (
                        "Audit done", 
                        { 
                            result 
                        }
                    );
                    
                    job.progress ( 100 );
                    done();
                }

                catch ( e ) {
                    console.log ( e.message )
                }            
            break;

            case 'key-module-access':
            break;
        }

        
    }
}

const am = new AnalyticsManager ();