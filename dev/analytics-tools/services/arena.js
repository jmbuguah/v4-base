
const Arena =  require ( 'bull-arena' );

const arenaConfig = Arena({
  queues: [
    {
      // Name of the bull queue, this name must match up exactly with what you've defined in bull.
      name: "analytics_queue",

      // Hostname or queue prefix, you can put whatever you want.
      hostId: "ANALYTICS@2018",

      // Redis auth.
      redis: {
        port: 6379,
        host: '127.0.0.1',
        password: ''
      },
    },
    {
      // Name of the bull queue, this name must match up exactly with what you've defined in bull.
      name: "redis_queue",

      // Hostname or queue prefix, you can put whatever you want.
      hostId: "REDIS@2018",

      // Redis auth.
      redis: {
        port: 6379,
        host: '127.0.0.1',
        password: ''
      },
    },
    {
      // Name of the bull queue, this name must match up exactly with what you've defined in bull.
      name: "audit_queue",

      // Hostname or queue prefix, you can put whatever you want.
      hostId: "AUDIT@2018",

      // Redis auth.
      redis: {
        port: 6379,
        host: '127.0.0.1',
        password: ''
      },
    }
  ],
},
{
  // Make the arena dashboard become available at {my-site.com}/arena.
  basePath: '/arena',

  // Let express handle the listening.
  disableListen: true
});


const express = require('express');
const app  = express();

// Make arena's resources (js/css deps) available at the base app route
app.use('/', arenaConfig);

app.listen ( 3000 )

