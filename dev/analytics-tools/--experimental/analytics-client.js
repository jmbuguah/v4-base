class AnalyticsClient { 

    constructor ( ) {
        this.Queue      = require('bull');
        this.queue      = new this.Queue("analytics_queue");
    }

    enQueue ( transaction ) {
        this.queue.add  ( transaction )
        this.queue.close();
    }

}

const ac = new AnalyticsClient ( );

let moment = require ( 'moment' )
const shortid = require('shortid'); 


let analyticsTransaction = {
    timestamp            : `${moment().unix()}`,
    type                 : 'transaction',
    name                 : 'balance enquiry',
    charge               : '20',
    amount               : '0',
    txid                 : shortid.generate().replace ( /[^a-zA-Z0-9]/g,'80X').toUpperCase(),
    userid               : '254721854426',
    ussdservice          : 'smep-ussd@0.0.1'
}

let auditTransaction     = {
    timestamp            : `${moment().unix()}`,
    input                : '',
    type                 : 'audit',
    menuAccessed         : 'Login',
    userid               : '254721854426',
    ussdservice          : 'smep-ussd@0.0.1'
}

console.time    ('runWithoutWorker')
ac.enQueue ( analyticsTransaction )
console.timeEnd    ('runWithoutWorker')


console.time    ('runWithoutWorker')
ac.enQueue ( auditTransaction )
console.timeEnd    ('runWithoutWorker')