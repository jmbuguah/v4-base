const { parentPort, workerData } = require ('worker_threads')
const transaction = workerData;

class AnalyticsClient { 

    constructor ( ) {
        this.Queue      = require('bull');
        this.queue      = new this.Queue("analytics_queue");
    }

    enQueue ( transaction ) {
        this.queue.add  ( transaction )
        this.queue.close();
    }

}

const ac = new AnalyticsClient ( transaction);
parentPort.postMessage ( `worker has worked`)

