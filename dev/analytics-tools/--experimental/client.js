const { 
    Worker, 
    isMainThread, 
    parentport, 
    workerData 
}                = require ('worker_threads')
const path       = require ('path')
const workerPath = path.resolve ( __dirname, 'client-worker.js')

let callWorker = ( data ) => {

    return new Promise ( ( resolve, reject ) => {

        const worker = new Worker ( workerPath, { workerData : data })

        worker.on ('message', resolve )
        worker.on ('error',   resolve )
        worker.on ('exit',    code => {
            reject ( new Error (`Worker stopped with exit code ${code}`))
        })
    })  

}

let test = async () => {

    let transaction = {
        name       : 'balance enquiry',
        charge     : '20',
        amount     : '0',
        txid       : 'AAAJJAKJHKJ785',
        userid     : '254721854426',
        ussdservice: 'smep-ussd@0.0.1'
    }
    console.time    ('runWorker')
    for ( let i = 0; i < 10; i ++ ) {
        let result = await callWorker ( transaction )
        // console.log ( { result } )
    }
    console.timeEnd    ('runWorker')

}


    //Run the Ussd as a Cluster
    let cluster = require('cluster');
    if (cluster.isMaster) {
        var workers = require('os').cpus().length;
        for (let i = 0; i < workers; i++) {
            cluster.fork();
        }
        cluster.on('online', (worker) => {
            console.log(`Nodejs Worker process { ${worker.process.pid} } is online.`);
        });
        cluster.on('exit', (worker, code, signal) => {
            cluster.fork();
        });
    }
    else {
        //run our http server
        test ();
    }



    


