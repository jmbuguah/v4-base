"use strict";
class ApiHandler {
    constructor(redisApi, connection, env) {
        this.apiTimeout = 3000;
        this.apiSettings = {};
        this.connection = {};
        this.env = '';
        this.apiSettings = redisApi;
        this.connection = connection;
        this.env = env;
    }
    async fetch(method, url, data) {
        const axios = require('axios');
        const instance = axios.create();
        instance.setTimeout = this.apiTimeout;
        let response = false;
        let result = false;
        let code = 0;
        try {
            switch (method) {
                case "get":
                    response = await instance.get(url, data).catch((e) => {
                        code = e.response.status;
                    });
                    break;
                case "post":
                    response = await instance.post(url, data).catch((e) => {
                        code = e.response.status;
                    });
                    break;
            }
            if (response) {
                code = response.status;
                result = response.data;
            }
            return {
                code,
                data: result
            };
        }
        catch (e) {
            return {
                code,
                data: result
            };
        }
    }
    async run(path, data) {
        try {
            let settings = this.apiSettings[this.env];
            let customware = require(`./api-middleware/api-custom-middleware/${settings['middleware']}`);
            let endpointUrl = `${this.connection.protocol}://${this.connection.host}:${this.connection.port}/${this.connection.path}`;
            //Run Request
            let payload = require('./api-middleware/api-core-middleware').request(settings, path, data, customware);
            console.log({
                request: JSON.parse(Buffer.from(payload.data, 'base64').toString())
            });
            if (payload.isValid) {
                let res = await this.fetch(this.connection.method, endpointUrl, payload.data);
                if (res.code === 200) {
                    //log the request and the response
                    console.log({
                        request: JSON.parse(Buffer.from(payload.data, 'base64').toString()),
                        response: JSON.parse(Buffer.from(res.data, 'base64').toString())
                    });
                    //decode the response
                    let responseObject = {
                        data: res.data,
                        base64: settings.base64,
                        encrypt: settings.encrypt,
                        format: settings["payload-format"],
                        utility: customware(settings)
                    };
                    //parse our response
                    let responseData = require('./api-middleware/api-core-middleware').response(responseObject);
                    //format our response accordingly
                    return customware()[`${path}-handler`](responseData, data);
                }
                else {
                    console.log('Request failed..');
                }
            }
            else {
                console.log(`The request contains missing Fields:\nX ${payload.err.join(',\nX ')}`);
            }
        }
        catch (e) {
            console.log(e);
        }
    }
}
module.exports = ApiHandler;
