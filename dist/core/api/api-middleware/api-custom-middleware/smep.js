"use strict";
var extensions = {
    encryptionHandler: (data) => {
        return `ENCRYPTED: ${data}`;
    },
    'login-handler': (data, requestData) => {
        /**
         * validate and transform the response :
         * the returned value has to contain status and message keys
         *
         * --------------------------------------------------------------
         *
         * Validate the response data and format it to the correct format
         *
         * --------------------------------------------------------------
         *
         */
        console.log(data);
        let responseIsValid = true;
        if (responseIsValid) {
            //get the wallet account
            let msisdn = '';
            for (let item of requestData) {
                if (item.name === 'msisdn') {
                    msisdn = item.value;
                }
            }
            //response mapping to standard USSD Mobile Banking format:
            let accountType = 'agent';
            let firstLogin = true;
            let isRegistered = false;
            let isBlocked = true;
            let linkedAccounts = [];
            let current_step = 'login';
            if (data.ACTIVE === '1') {
                isBlocked = false;
            }
            if (data.field39 === '00') {
                isRegistered = true;
            }
            if (data.FIRSTLOGIN === '0') {
                firstLogin = false;
            }
            if (data.MPESA_AGENT === '0') {
                accountType = 'client';
            }
            if (data.linkedAccounts) {
                let accounts = data.linkedAccounts.split('|');
                accounts = accounts.filter((item) => {
                    return item.trim() !== '' && typeof (item) !== 'undefined';
                });
                accounts = accounts.map((account) => {
                    return {
                        label: `Sacco Account - ${account}`,
                        value: account
                    };
                });
                linkedAccounts = accounts;
            }
            if (!isRegistered) {
                isBlocked = false;
            }
            let userData = {
                "account-details": {
                    "account-type": accountType,
                    "identification-id": data.IDENTIFICATIONID || '',
                    "is-registered": isRegistered,
                    "is-blocked": isBlocked,
                    "first-login": firstLogin,
                    "firstname": data.FIRSTNAME || '',
                    "surname": data.LASTNAME || '',
                    "othername": data.SECONDNAME || '',
                    "fullname": data.CUSTOMERNAME || '',
                    "active": data.ACTIVE || '',
                    "loan-limit": `${parseInt(data.LOANSCORED, 10)}` || '0',
                    "pin": data.PIN || '',
                    "linked-accounts": linkedAccounts,
                    "owned-accounts": [
                        {
                            "label": "Mum",
                            "value": "1002342"
                        },
                        {
                            "label": "Dad",
                            "value": "89789789"
                        }
                    ],
                    "current-loans": [
                        {
                            "id": "3001",
                            "name": "Smep Wallet Loan",
                            "due-date": "12-Dec-2020",
                            "balance": "400",
                            "total-amount": "800",
                            "percentage-interest": "10"
                        }
                    ],
                    "school-options": [
                        {
                            "label": "Moi Girls Nairobi",
                            "value": "001"
                        },
                        {
                            "label": "lenana School",
                            "value": "002"
                        },
                        {
                            "label": "Mangu High School",
                            "value": "003"
                        },
                        {
                            "label": "Sunshine School",
                            "value": "004"
                        },
                        {
                            "label": "Nairobi School",
                            "value": "005"
                        },
                        {
                            "label": "St Georges Academy",
                            "value": "006"
                        },
                        {
                            "label": "Aquinas School",
                            "value": "007"
                        }
                    ],
                    "search-options": [
                        {
                            "label": "lenana School",
                            "value": "002"
                        }
                    ]
                },
                "app_description": "the CFL USSD",
                "app_name": "smep-ussd@0.0.1",
                "language": "swahili",
                "msisdn": data.PHONENUMBER || msisdn,
                "email": data.EMAILADDRESS || '',
                "terms_url": "info@smep.co.ke",
                "pin-trials-remaining": "3",
                "global-constants": {
                    "funds_transfer_minimum": 50,
                    "airtime_topup_minimum": 10,
                    "language-options": [
                        {
                            "label": "english-label",
                            "value": "english"
                        },
                        {
                            "label": "swahili-label",
                            "value": "swahili"
                        },
                        {
                            "label": "french-label",
                            "value": "french"
                        }
                    ],
                    "yes-no-options": [
                        {
                            "label": "yes-label",
                            "value": "1"
                        },
                        {
                            "label": "no-label",
                            "value": "2"
                        }
                    ],
                    "confirm-options": [
                        {
                            "label": "confirm-label",
                            "value": "1"
                        },
                        {
                            "label": "cancel-label",
                            "value": "2"
                        }
                    ]
                },
                "global-request-details": {}
            };
            return {
                status: 'success',
                message: userData
            };
        }
        else {
            return {
                status: 'failed',
                message: ''
            };
        }
    },
    'registration-handler': (data) => {
        if (data.field39 === '00') {
            return {
                status: 'success',
                statusCode: data.field39,
                message: data.field48
            };
        }
        else {
            return {
                status: 'failed',
                statusCode: data.field39,
                message: data.field48
            };
        }
    },
    'pin-change-handler': (data) => {
        if (data.field39 === '00') {
            return {
                status: 'success',
                statusCode: data.field39,
                message: data.field48
            };
        }
        else {
            return {
                status: 'failed',
                statusCode: data.field39,
                message: data.field48
            };
        }
    },
    'milk-supplied-handler': (data) => {
        if (data.field39 === '00') {
            return {
                status: 'success',
                statusCode: data.field39,
                message: data.field48
            };
        }
        else {
            return {
                status: 'failed',
                statusCode: data.field39,
                message: data.field48
            };
        }
    }
};
module.exports = (apiSettings = {}) => {
    return require('../api-core-middleware').utilities(apiSettings).extend(extensions);
};
