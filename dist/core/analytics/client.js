"use strict";
class AnalyticsClient {
    constructor() {
        this.Queue = require('bull');
        this.queue = new this.Queue("analytics_queue");
    }
    enQueue(transaction) {
        this.queue.add(transaction);
        this.queue.close();
    }
}
module.exports = AnalyticsClient;
