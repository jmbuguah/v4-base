"use strict";
class Ussd {
    constructor() {
        this.app = require('express')().disable('x-powered-by');
        this.cors = require('cors');
        this.morgan = require('morgan');
        this.port = 8008;
        this.csrf = require('csurf');
        this.csrfProtection = this.csrf({ cookie: true });
        this.cookieParser = require('cookie-parser');
    }
    //cache middleware
    async cacheMiddleware(request, response, next) {
        /**
        * --------------------------------------
        *
        * PARSE THE MNO REQUEST PARAMS
        *
        * --------------------------------------
        */
        //get the relevant query items
        let app_name = decodeURI(request.params.app);
        let user_id = decodeURI(request.query.MOBILE_NUMBER);
        //filter the user input string from the MNO
        let BODY_DELIMITER = '*';
        let ussd_body = request.query.USSD_BODY.split(BODY_DELIMITER);
        ussd_body = ussd_body.filter((item) => {
            return item && typeof (item) !== 'undefined' && item.trim() !== '' && item !== null;
        });
        let user_input = ussd_body.length === 0 ? '' : ussd_body[ussd_body.length - 1];
        //initiate our data holder variables
        let cache_id = [app_name, 'clients', user_id].join(':');
        let requestObject = {
            user_input: user_input,
            user_data: false,
            application_data: false
        };
        /**
         * --------------------------------------
         *
         * FETCH THE USER & APP DATA FROM CACHE
         *
         * --------------------------------------
         */
        //[ Analytics ] Log Redis Response time
        console.time('Redis fetch duration');
        //fetch the users data if it exists
        let app_data = false;
        let cache = require('./core/cache/cache');
        let store = new cache();
        let keys = [
            [app_name, 'config', 'api'].join(':'),
            [app_name, 'config', 'config'].join(':'),
            [app_name, 'config', 'language'].join(':'),
            [app_name, 'config', 'pages'].join(':'),
            [app_name, 'config', 'prompts'].join(':'),
            [app_name, 'config', 'prompts_cache'].join(':'),
            cache_id
        ];
        let redis_data = await store.getMany(keys);
        let api = redis_data.api;
        let config = redis_data.config;
        let language = redis_data.language;
        let pages = redis_data.pages;
        let prompts = redis_data.prompts;
        let prompts_cache = redis_data.prompts_cache;
        let user_data = redis_data[`${user_id}`];
        //set user data to false in case we get a blank object from redis
        if (typeof user_data === 'object' && Object.keys(user_data).length === 0) {
            user_data = false;
        }
        console.timeEnd('Redis fetch duration');
        //all app data exists
        if (api && config && language && pages && prompts && prompts_cache) {
            requestObject.application_data = {
                api,
                config,
                language,
                pages,
                prompts,
                prompts_cache
            };
            app_data = true;
        }
        //user data exists and app data exists and it isnt the first request
        if (user_data && app_data && user_input !== '') {
            requestObject.user_data = user_data;
            requestObject['cache_id'] = cache_id;
            request.query = requestObject;
            request.query.hasData = true;
        }
        //app data doesnt exist
        else if (!app_data) {
            console.log('App doesnt exist...');
            request.query.hasData = false;
        }
        //user data isnt in redis or its the first request        
        else if (!user_data && app_data || user_input === '' && app_data) {
            let isOffline = request.app.locals.offline;
            if (isOffline) {
                //add to our user object
                requestObject.user_data = user_data;
                requestObject['cache_id'] = cache_id;
                requestObject.app_env = request.app.locals.api_env;
                request.query = requestObject;
                request.query.hasData = true;
            }
            else {
                console.time('ESB duration');
                let Api = require('./core/api/');
                let api_connection_params = config['data-source'][request.app.locals.api_env];
                let apiHandler = new Api(api, api_connection_params, request.app.locals.api_env);
                let apiResult = await apiHandler.run('login', [{ name: 'walletAccount', value: user_id }, { name: 'msisdn', value: user_id }]);
                //on-success, persist user data
                if (apiResult.status === 'success') {
                    let data = apiResult.message;
                    //persist the users pin trials and language choice
                    if (user_input === '' && user_data) {
                        data.language = user_data.language;
                        data['pin-trials-remaining'] = user_data['pin-trials-remaining'];
                    }
                    //persist to redis
                    await store.put(cache_id, data);
                    //add to our user object
                    requestObject.user_data = data;
                    requestObject['cache_id'] = cache_id;
                    requestObject.app_env = request.app.locals.api_env;
                    request.query = requestObject;
                    request.query.hasData = true;
                }
                else {
                    //if the data is valid, persist it to redis and pass it on to the ussd for processing
                    request.query.hasData = false;
                }
                console.timeEnd('ESB duration');
            }
        }
        //Go to the next middleware
        next();
    }
    //menu handler middleware
    async menuHandlerMiddleware(request, response, next) {
        if (request.query.hasData) {
            let Handler = require('./core/handler/');
            let handler = new Handler(request.query);
            let response = await handler.run();
            if (response) {
                request.query["menuResponse"] = response;
            }
            else {
                request.query["menuResponse"] = `An Menu Handling error - \`menuHandlerMiddleware\``;
            }
        }
        else {
            //use menu handler functionalities in order to load an error prompt
            request.query["menuResponse"] = `Cannot get user data from Cache or Api`;
        }
        //go to the next middleware
        next();
    }
    //Rest handler
    sendMnoResponse(request = {}, response = '') {
        //normalize the string as special characters in French are not displayed properly on USSD
        let normalize = true;
        let responseString = request.query.menuResponse;
        if (normalize) {
            let norm = require('normalize-strings');
            responseString = norm(responseString);
        }
        //format the response based on the MNO
        response.send(responseString);
    }
    //Http Server
    async runServer() {
        //allowing cross origin requests
        this.app.use(this.cors());
        //allowing morgan logging
        // this.app.use    ( this.morgan ( 'combined' ) )
        //OWASP protection
        this.app.use(this.cookieParser());
        this.app.use(this.csrfProtection);
        this.app.use((request, response, next) => {
            response.set('X-Content-Type-Options', 'nosniff');
            response.set('X-Frame-Options', 'SAMEORIGIN');
            response.set('Content-Security-Policy', "frame-ancestors 'none'");
            next();
        });
        //some env settings
        this.app.locals.api_env = 'smep';
        this.app.locals.offline = false;
        //Register endpoint to handle all POST and GET requests
        this.app.all('/v4/:app', [
            this.cacheMiddleware,
            this.menuHandlerMiddleware
        ], this.sendMnoResponse);
        //listen on the selected port
        this.app.listen(this.port, (err) => {
            if (err)
                throw err;
            console.log('The USSD v4 Engine is Listening on port %d', this.port);
        });
    }
    //Cluster
    async runCluster() {
        //Run the Ussd as a Cluster
        let cluster = require('cluster');
        if (cluster.isMaster) {
            var workers = require('os').cpus().length - 1; //Saved one core for the processor
            for (let i = 0; i < workers; i++) {
                cluster.fork();
            }
            cluster.on('online', (worker) => {
                console.log(`Nodejs Worker process { ${worker.process.pid} } is online.`);
            });
            cluster.on('exit', (worker, code, signal) => {
                cluster.fork();
            });
        }
        else {
            //run our http server
            this.runServer();
        }
    }
}
//Run our USSD
let ussd = new Ussd();
ussd.runCluster();
