const coreMiddleware           = {
    /**
     * REQUEST
     */
    getRequestTemplate  : ( requestObject:any ) => {

        //destructure our request object
        let { defaultTemplate, requestTemplate, requestQueryArray, metaData, utility  } = requestObject;

        //final template
        let  finalTemplate:any = {};

        //validate 
        let validate :any = coreMiddleware.requestValidate ( requestQueryArray, requestTemplate )

        if ( validate.isValid ) {

            //replace defaults in the default template
            let keys  : string [] = Object.keys ( defaultTemplate );

            for ( let key of keys ) {
                if ( defaultTemplate [ key ].includes ( ':' ) ) {
                    let argsArray                 = defaultTemplate [ key ].replace( /\s/g,'').split(':');
                    let action                    = argsArray [ 0 ]
                    let argName                   = argsArray [ 1 ];
                    let arg                       = false;    
                    if ( argName.includes ( '=' ) ) {
                        let args                  = argName.split ( '=' )
                        argName                   = args [ 0 ]
                        arg                       = args [ 1 ]
                    }
                    let replacement               = coreMiddleware.requestDefaults ( action, argName, arg, utility )
                    finalTemplate [ key ]         = replacement;                
                }
                else {
                    finalTemplate [ key ]         = defaultTemplate [ key ]
                }
            }

            //merge
            finalTemplate  = coreMiddleware.requestMerge ( finalTemplate, validate.replaced ) 

            //format
            finalTemplate  = coreMiddleware.requestFormat ( metaData [ 'payload-format'], finalTemplate, utility )            

            //encode
            if ( metaData [ 'base64'] ){
                finalTemplate  = coreMiddleware.requestEncode ( 'base64', finalTemplate, utility )
            } 

            if ( metaData [ 'encrypt'] ){
                finalTemplate  = coreMiddleware.requestEncrypt ( `encryptionHandler`, finalTemplate, utility )
            } 

            
            
            finalTemplate = {
                isValid: true,
                data: finalTemplate
            }
        }
        else {
            finalTemplate = {
                isValid: false,
                err: validate.err
            }
        }

        return finalTemplate;

        
    },
    requestDefaults     : ( action: string, methodName: string,args: any = false,utility: any ) => {

        
        let replacement = '';
        
        if ( action === 'create' ) {
            if ( args ) {
                replacement = utility[ `${methodName}`] ( args )
            }  
            else {
                replacement = utility[ `${methodName}`] ( )
            } 
        }

        if ( action === 'get' ) {
            replacement = utility[ `get`] ( methodName )
        }

        return replacement;
        
    },
    requestValidate     : ( requestQueryArray: any[], requestObj: any ) => {

        //instantiate some params
        let passedValidation     : boolean   = false;
        let failedValidationArray: string [] = [];
        

        //get all the required keys that are prefixed with the `$` symbol
        let requiredKeys: string [] = []
        let requiredValues      : string [] = Object.values ( requestObj )
        for ( let value of requiredValues ) {
            if ( value.startsWith ( '$' ) ) {
                requiredKeys.push ( value.trim() )
            }

            //what if the $value is contained in a string representation?
            if ( value.includes ( '$' )) {
                let splits = value.split('$')
                requiredKeys.push ( '$' + splits[1].trim() )
            }
        }

        //seperate the requestQueryArray into keys and values
        let requestQueryKeys  : any = [];
        let requestQueryValues: any = [];

        for ( let requestparam of requestQueryArray ) {
            requestQueryKeys.push   ( requestparam.name  )
            requestQueryValues.push ( requestparam.value )
        }

        //match and filter
        for ( let key of requiredKeys ) {
            let formattedKey = key.replace( /\$/g,'' )
            if ( requestQueryKeys.includes ( formattedKey ) ) {                
            }
            else {
                failedValidationArray.push ( formattedKey )
            }
            
        }

        if ( failedValidationArray.length === 0 ) {
            //replace the default values 
            for ( let requestparam of requestQueryArray ) {
                let name = '$' + requestparam.name  
                let value = requestparam.value;

                //find the value of the request object to replace
                let keys = Object.keys ( requestObj ) 

                for ( let key of keys ) {
                    let requestObjectValue = requestObj [ key ]
                    if ( requestObjectValue === name ) {
                        requestObj [ key ] = value
                    }

                    if ( requestObjectValue.includes(name) ) {
                        requestObj [ key ] = requestObj [ key ].replace ( name,value )
                    }

                }
            }
            
            
            return {
                isValid: true,
                replaced: requestObj
            }
        }
        else{
            return {
                isValid: false, 
                err    : failedValidationArray
            }
        }

        

    },
    requestMerge        : ( defaults: object, request: Object ) => {
        var sortObj = require('sort-object');
        let merged  = { ...defaults, ...request }
        return sortObj ( merged )
    }, 
    requestFormat       : ( format: string, data: any, utility:any ) => {
        return utility [ format ] ( data )
    },
    requestEncode       : ( encType: string, data: any, utility:any ) => {
        return utility [ encType ] ( data )
    },
    requestEncrypt      : ( encryptionHandler: string, data: string, utility:any ) => {
        return utility [ encryptionHandler ] ( data )
    },

    /**
     * RESPONSE
     */
    getResponseObject   : ( responseObject:any ) => {

        let { data, base64, encrypt, format, utility } = responseObject;

        if ( base64 ) {
            data = coreMiddleware.responseDecode ( 'base64Decode', data, utility )
        }

        data = coreMiddleware.responseParse ( `from${format}`, data, utility )

        return data;

    },
    responseDecode      : ( encType: string, data: any, utility:any ) => {
        return utility [ encType ] ( data )
    },
    responseDecrypt     : () => {
    },    
    responseParse       : ( format: string, data: any, utility:any ) => {
        return utility [ format ] ( data )
    }   
}

const utilities                = ( settings:any ) => { 
    
    let defaults = {
        data                : settings,
        base64              : ( payload:string )   => {
            let payloadToBase64 = Buffer.from (payload).toString('base64');
            return payloadToBase64 ;
        },
        base64Decode        : ( payload:string )   => {
            let payloadFromBase64 = Buffer.from( payload, 'base64');
            return payloadFromBase64 ;
        },
        timeStamp           : ( arg: string ) => {
            try {
                let moment = require ( 'moment' );
                let formatted = moment().format( arg )
                return formatted
            }
            catch ( e ) {
                return false
            }            
        },    
        JSON : ( data:any ) => {
            return JSON.stringify ( data, null, 4 )
        },
        fromJSON : ( data:any ) => {
            try {
                return JSON.parse ( data )
            }
            catch ( e ) {
                return data
            }
        },
        XML : ( data:any ) => {
            var xml :any = false; 
            if (typeof(data) === "object") {

                xml = `<?xml version= "1.0" encoding="utf-8"?>\n<message>`;

                let keys = Object.keys ( data );

                for ( let key of keys) {
                    xml += `\n\t<${key}>${data [ key ]}</${key}>`;
                }
                xml += `\n</message>`;          

            }
            return  xml ;
        },
        stan                : ()   => {            
            let randomInt = Math.floor(Math.random() * (999999 - 1 + 1))
            return String("000000" + randomInt ).slice(-1*6);
        },    
        transactionId       : ()   => {
            const shortid = require ( 'shortid')
            return shortid.generate().replace ( /[^a-zA-Z0-9]/g,'80X').toUpperCase();
        },
        get                 : ( arg:any )   => {
            return defaults.data [ arg ]
        },
        extend              : ( extensions:any ) => {
            return { ...defaults, ...extensions }
        }
    }

    return defaults;
}

const request                  = ( settings:any, path:string, queryArray: object [], utility:any )  => {

        let middlewareObject :any  = {
            defaultTemplate     : settings [ `request-template` ], 
            requestTemplate     : settings [ `endpoints` ] [ path ] [ 'request' ],
            requestQueryArray   : queryArray,
            metaData            : {
                "payload-format": settings [ "payload-format"],
                "base64"        : settings [ "base64"],
                "encrypt"       : settings [ "encrypt"],
                "metaData"      : settings [ "meta-data"]
            },
            utility             : utility ( settings )            
        }
        let payload           :any = coreMiddleware.getRequestTemplate ( middlewareObject )
        return payload;
}

const response                 = ( responseObject:any )  => {

    return coreMiddleware.getResponseObject ( responseObject )
}
module.exports =  { request, response, utilities }