var extensions : any = {
    encryptionHandler: ( data:object ) => {
        return `ENCRYPTED: ${data}`
    },
    'user-data-handler': ( data:any ) => {
        /**
         * validate and transform the response :
         * the returned value has to contain status and message keys
         */
        data.msisdn = '254729347882'

        return {
            status: 'success',
            message: data
        }
    },
    'smep-user-data-handler': ( data:any ) => {
        /**
         * validate and transform the response :
         * the returned value has to contain status and message keys
         * 
         * --------------------------------------------------------------
         * 
         * Validate the response data and format it to the correct format
         * 
         * --------------------------------------------------------------
         *  {
                MPESA_AGENT         : '0',
                LOANAMOUNT          : '0',
                ACTIVE              : '1',
                PHONENUMBER         : '254729347882',
                CLOSED              : '0',
                SECONDNAME          : 'MBUGUAH',
                customerLoan        : '0',
                EMAILADDRESS        : 'mburu.johana@ekenya.co.ke',
                FIRSTNAME           : 'JOHANA',
                field39             : '00',
                billers             : 'SAFARICOM~AIRTIME~AIRTIME|HOUSERENT~HOOUSE RENT~BILLPAYMENT|NHIF~NHIF~BILLPAYMENT|LANDRATES~NAIROBI CITY LANDARATES~BILLPAYMENT|NAIROBIWATER~NAIROBI CITY NAIROBI WATER~BILLPAYMENT|SINGLEBUSINESSPERMIT~NAIROBI CITY SINGLE BUSSINESS PERMIT~BILLPAYMENT|MISC~NAIROBI CITY MISCELENIOUS~BILLPAYMENT|DAILYPARKING~DAILY PARKING ~BILLPAYMENT|AIRTEL~AIRTIME~AIRTIME|TELKOM_AIRTIME~AIRTIME~AIRTIME|SEASONALPARKING~NCC SEASONAL PARKING ~BILLPAYMENT|KPLCPREPAID~KPLCPREPAID~BILLPAYMENT|KPLCPOSTPAID~KPLCPOSTPAID~BILLPAYMENT|DSTV~DSTV~BILLPAYMENT|GOTV ~GOTV ~BILLPAYMENT|SACCOPARKING~SACCOPARKING~BILLPAYMENT|FAIBA~For Internet~BILLPAYMENT|',
                LANG                : 'EN        ',
                field48             : 'Successful',
                LASTNAME            : 'MBURU',
                IMSI                : '2861283172',
                GENDER              : 'M',
                IDENTIFICATIONID    : '27652279',
                ID                  : '57',
                PARTIAL_REGISTRATION: '0',
                CREATEDON           : '2018-11-05 00: 10: 00.000000 +00: 00',
                ACCOUNTNAME         : 'JOHANA MBUGUAH MBURU',
                loanProducts        : '3001~SMEP E-WALLET LOAN~10.00~1~100.00~50000.00~0|',
                FIRSTLOGIN          : '0',
                PIN                 : '873a820fbb5d58da128def2c89198b91f8670b486e853c056a7bba3e732595e6',
                DATEOFBIRTH         : '2018-05-28',
                field37             : 'USSD18280021417',
                CUSTOMERNO          : '254729347882',
                ACCOUNTSTATUS       : '1',
                LOANSCORED          : '0.00',
                CLOSEACCOUNT        : '0',
                linkedAccounts      : '11987443|',
                MWALLETACCOUNT      : '254729347882',
                APPROVED            : '1',
                CUSTOMERNAME        : 'JOHANA MBUGUAH MBURU',
                LOANSTATUS          : '0',
                field2              : '254729347882' 
            }
         */
        let responseIsValid = true;

        if ( responseIsValid ) {

            //response mapping to standard USSD Mobile Banking format:
            let accountType    = 'agent';
            let firstLogin     = true;
            let isRegistered   = false;
            let isBlocked      = true;
            let linkedAccounts = [];

            if ( data.ACTIVE      === '1'  ){
                isBlocked = false;
            }
            if ( data.field39     === '00' ) {
                isRegistered = true;
            }
            if ( data.FIRSTLOGIN  === '0'  ) {
                firstLogin = false;
            }
            if ( data.MPESA_AGENT === '0'  ) {
                accountType = 'client';
            }
            if ( data.linkedAccounts       ) {
                let accounts = data.linkedAccounts.split ( '|' );
                accounts = accounts.filter ( ( item:string ) => {
                    return item.trim() !== '' && typeof(item) !== 'undefined'
                })

                accounts = accounts.map ( (account:any ) => {
                    return {
                        label:`Sacco Account - ${account}`,
                        value: account
                    }
                })

                linkedAccounts = accounts
            }


            let   userData = {
                "account-details"                : {        
                    "account-type"               : accountType,
                    "identification-id"          : data.IDENTIFICATIONID,
                    "is-registered"              : isRegistered,
                    "is-blocked"                 : isBlocked,
                    "first-login"                : firstLogin,
                    "firstname"                  : data.FIRSTNAME,
                    "surname"                    : data.LASTNAME,
                    "othername"                  : data.SECONDNAME || '',
                    "fullname"                   : data.CUSTOMERNAME,
                    "active"                     : data.ACTIVE,
                    "loan-limit"                 : `${parseInt ( data.LOANSCORED,10 )}` || '0',
                    "pin"                        : data.PIN,
                    "linked-accounts"            : linkedAccounts,        
                    "owned-accounts"             : [
                        {
                            "label"              : "Mum",
                            "value"              : "1002342"
                        },
                        {
                            "label"              : "Dad",
                            "value"              : "89789789"
                        }
                    ],
                    "current-loans"              : [
                        {
                            "id"                 : "3001",
                            "name"               : "Smep Wallet Loan",
                            "due-date"           : "12-Dec-2020",
                            "balance"            : "400",
                            "total-amount"       : "800",
                            "percentage-interest": "10"
                        }
                    ],
                    "school-options"             : [
                        {
                            "label"              : "Moi Girls Nairobi",
                            "value"              : "001"
                        },
                        {
                            "label"              : "lenana School",
                            "value"              : "002"
                        },
                        {
                            "label"              : "Mangu High School",
                            "value"              : "003"
                        },
                        {
                            "label"              : "Sunshine School",
                            "value"              : "004"
                        },
                        {
                            "label"              : "Nairobi School",
                            "value"              : "005"
                        },
                        {
                            "label"              : "St Georges Academy",
                            "value"              : "006"
                        },
                        {
                            "label"              : "Aquinas School",
                            "value"              : "007"
                        }
                    ],
                    "search-options"             : [
                        {
                            "label"              : "lenana School",
                            "value"              : "002"
                        }
                    ]
                },
                "app_description"                : "SMEP MFI",
                "app_name"                       : "smep-ussd@0.0.1",
                "language"                       : "english",
                "msisdn"                         : data.PHONENUMBER,
                "email"                          : data.EMAILADDRESS,
                "pin-trials-remaining"           : "3",  
                "global-constants"               : {
                    "funds_transfer_minimum"     : 50,
                    "airtime_topup_minimum"      : 10,
                    "language-options"           : [
                        {
                            "label"              : "english-label",
                            "value"              : "english"
                        },
                        {
                            "label"              : "swahili-label",
                            "value"              : "swahili"
                        },
                        {
                            "label"              : "french-label",
                            "value"              : "french"
                        }
                    ],
                    "yes-no-options"             : [
                        {
                            "label"              : "yes-label",
                            "value"              : "1"
                        },
                        {
                            "label"              : "no-label",
                            "value"              : "2"
                        }
                    ],
                    "confirm-options"            : [
                        {
                            "label"              : "confirm-label",
                            "value"              : "1"
                        },
                        {
                            "label"              : "cancel-label",
                            "value"              : "2"
                        }
                    ]
                },
                "global-request-details"        : {}
            }

            return {
                status: 'success',
                message: userData
            }
        }
        else {
            return {
                status: 'failed',
                message: ''
            }
        }
    }
}

module.exports = ( apiSettings:object = {} ) => {
    return require ( '../api-core-middleware').utilities( apiSettings ).extend( extensions )
}