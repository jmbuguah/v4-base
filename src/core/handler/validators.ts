let isAmount =        ( Joi: any, min: any ) => {
    let minInt = parseInt ( min, 10 );
    return ( {
        isAmount: Joi.number().required().min( minInt )
    });
};

let isText =        ( data:any ) => {
    let dataLength    = data.length;
    //let regex         = /[^a-zA-Z]/g;
    let regex         = /^[A-Za-z]+$/i;
    let newData       = data.replace ( regex, "" );
    let newDataLength = newData.length;

    if ( regex.test(data) ) {

        return true;
    }
    else {
        return false;
    }
};

let isNumber =        ( joi: any ) => {
    return ( {
        isNumber: joi.number().required()
    });
};

let checkIfIsNumber  = ( data:any ) => {
    //isNaN

    let dataLength    = data.length;
    let regex         = /[^0-9]/g;
    let newData       = data.replace ( regex, "" );
    let newDataLength = newData.length;

    if ( dataLength == newDataLength ) {
   // if ( regex.test(data) ) {
        return true;
    }
    else {
        return false;
    }        
}

let isEmail = ( data:any ) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let isEmail = re.test(data);

    if (!isEmail && data.toLowerCase() ==='s' ) {
        return true;
    }
    else {
        return isEmail;
    }
}

let isCorrectPin =    ( data:any, user_data:any ) => {

    let oldPin = user_data ['account-details'].pin;
    let msisdn = user_data .msisdn;

    //SMEP hashing function
    let CryptoJS     = require ("crypto-js");
    let newPin = CryptoJS.HmacSHA256( Buffer.from ( data + msisdn ).toString('base64') , "secret").toString(CryptoJS.enc.Hex);

    if ( oldPin === newPin ) {
        return true;
    }
    else {
        return false;
    }
}

let isDate = ( data:any ) => {
    //format DD-MM-YYYY
    let wDashes = data.replace( /-/g,'')

    //total length is correct
    if ( wDashes.length !== 8 ) {
        return false
    }
    let splitData = data.split ( '-' );

    //parts are the correct length
    if ( splitData [0].length !== 2 ) {
        return false;
    }
    if ( splitData [1].length !== 2 ) {
        return false;
    }
    if ( splitData [2].length !== 4 ) {
        return false;
    }

    //contains only numbers
    if ( !checkIfIsNumber (splitData [0])) {
        return false;
    }
    if ( !checkIfIsNumber (splitData [1])) {
        return false;
    }
    if ( !checkIfIsNumber (splitData [2])) {
        return false;
    }

    //is a valid date
    if ( parseInt (splitData [0], 10 ) > 31 ) {
        return false;
    }

    if ( parseInt (splitData [1], 10 ) > 12 ) {
        return false;
    }

    return true;
}

let checkPinStrength :any = ( pinString:string ) => {

    //invalid length
    if ( pinString.length !== 4 ) {
        return {"valid": false, "msg": "invalid_pin_length"};
    }

    //Convert the PIN into an array
    let pin :any = [];
    for ( let p = 0 ; p < pinString.length ; p ++ ){
        pin.push ( pinString.charAt ( p ) );
    }

    //helper function to map only unique values
    let onlyUnique = ( value:any, index:any, self:any ) => { 
        return self.indexOf(value) === index;
    };


    try {
        let numbers       = '1,2,3,4,5,6,7,8,9,0';
        let numericArray  = numbers.split( ',');
        let totinvalid    = pin.length;
        let unique        = pin.filter( onlyUnique );

        //Ensures all digits are numbers
        for ( let i = 0; i < totinvalid; i++ ) {       
            
            if ( numericArray.indexOf ( pin[i] ) == -1 ) {
                return {"valid": false, "msg": "alpha_characters_found"};
            }
        }

        //Ensures that the pin doesnt consist of a single character
        let uniquePin = pin.filter( onlyUnique );



        //Ensure no sequence passwords exist e.g 1234, 4321, 9876,6789
        let sequence      = [];
        for ( let x = 0 ; x < pin.length ; x ++ ) {
            let difference :any = pin [ x ] - pin [ x + 1 ];
            sequence.push ( difference );
        }
        
        let sequenceDetected = sequence.filter ( onlyUnique );

        if ( sequenceDetected.length == 1 && sequenceDetected [ 0 ].toString ( ) === '1' ) {
            return {"valid": false, "msg": "easy_pin"};
        }

        if ( sequenceDetected.length == 1 && sequenceDetected [ 0 ].toString ( ) === '0' ) {
            return {"valid": false, "msg": "easy_pin"};
        }

        if ( sequenceDetected.length == 1 && sequenceDetected [ 0 ].toString ( ) === '-1' ) {
            return {"valid": false, "msg": "easy_pin"};
        }

        

        //ensure that no birthdays are entered
        let birthdays = [];
        let date      :any = new Date();
        let year      :any = date.getFullYear();
        let maxyear   :any = parseInt ( year ) - 18;
        let minyear   :any = parseInt  ( year ) - 100;

        for ( let t = minyear ; t <= maxyear; t++ ) {
            birthdays.push ( t );
        }

        if ( birthdays.indexOf ( parseInt ( pinString ) ) != -1 ) {
           return {"valid": false, "msg": "birthday_found"};
        }

        return {"valid": true, "msg": "valid_pin"};
    } 
    
    catch ( e ) {
    }
    
}


//check PIN strength
let isValidPin =      ( data:any ) => {

    if ( checkPinStrength( data ).valid ) {
        return true
    }

    else {
        return false
    }
}



module.exports = {
    isAmount,
    isText,
    isNumber,
    isEmail,
    isCorrectPin,
    isDate,
    isValidPin
}