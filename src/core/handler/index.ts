
interface ReqObj {
    user_data    : object
    user_input   : string
}

class MenuHandler {

    private path              : any    = require ( 'path' ); 

    //user data    
    private app_name          : string = '';    
    private app_path          : any    = './www';
    private cache_id          : any    = '';
    private current_step      : string = '';
    private user_data         : any    = {};
    private user_input        : any    = '';

    //app data
    private api               : any    = {};
    private app_config        : any    = {};
    private language          : any    = ''; 
    private pages             : any    = {};
    private prompts           : any    = {};
    private prompts_cache     : any    = {};
    private app_env           : any    = '';
    
    //others
    private PREVIOUS_CHARACTER: any    = "00";
    private HOME_CHARACTER    : any    = "000";
    private SKIP_CHARACTER    : string = 's';
    private analytics         : any = require ( '../analytics/client' );
    

    constructor ( reqObj : any ) {

        //load user data
        this.user_data        = reqObj.user_data;
        this.app_env          = reqObj.app_env;
        this.user_input       = reqObj.user_input;
        this.app_name         = this.user_data [ "app_name" ];
        this.app_path         = this.path.resolve ( __dirname, "..", "..", "..", 'www' );
        this.cache_id         = `${this.user_data [ "app_name" ]}:clients:${this.user_data [ "msisdn" ]}`.replace ( /\s/g,'');
        this.current_step     = this.user_data [ "current_step" ]; 
        
        //load app data
        this.api              = reqObj.application_data.api;
        this.app_config       = reqObj.application_data.config;
        this.language         = reqObj.application_data.language;
        this.pages            = reqObj.application_data.pages;
        this.prompts          = reqObj.application_data.prompts;
        this.prompts_cache    = reqObj.application_data.prompts_cache;

        //load all keys in the language file
        let langKeys       : any = Object.keys ( this.language );
        let newLanguageFile: any = {
            'english': {}, 
            'swahili': {},
            'french' : {}
        }
        let languages : any = Object.keys ( newLanguageFile );

        for ( let language of languages ) {
            for ( let langKey of langKeys ) {                
                newLanguageFile [ language ]  = { ...newLanguageFile [ language ], ... this.language [ langKey ] [ language ]}
            }
        }

        this.language = newLanguageFile;
    }
    
    async run                     () {        

        //On First Request Set Start Menu
        if ( this.user_input.trim () === '' ) { this.setStartMenu  () }
 
        //Load previous, current and next step data
        let data: any = this.loadData ();

        //Handle Menu
        let str = await this.route ( data, this.user_input );


        //Audit trail log
        let moment = require ( 'moment' )
        let chalk = require ( 'chalk' )

        console.log ( 
            chalk.green (`[ Audit Trail ]`), 
            chalk.cyan (` [ Time: ${ moment().format()  } ] - ${this.current_step }`) 
        )

        let ac = new this.analytics ();

        //add to mongo db audit queue
        ac.enQueue ( {
            timestamp            : `${moment().unix()}`,
            input                : this.user_input,
            type                 : 'audit',
            menuAccessed         : str,
            userid               : this.user_data.msisdn,
            ussdservice          : this.app_name
        })
 
        return str;
    }
    
    async route                   ( data: any, input:any = this.user_input ) {
        
        //initialize our variables
        let str                : any     = '';
        let canGoBack          : boolean = data.previous || false;
        let DEFAULT_MENU_ACTION: any     = 'routeToCurrent';
        let menuAction         : any     = DEFAULT_MENU_ACTION;
        
        //determine the menu action
        if ( input.trim() === '' ){  
            menuAction = 'routeToStart' 
        }
        if ( input.trim() === this.PREVIOUS_CHARACTER && canGoBack ){ 
            menuAction = 'routeToPrevious' 
        }
        // if ( input.trim() === this.HOME_CHARACTER ) {
        //     menuAction = 'routeToHome'
        // }

        switch ( menuAction ){

            case 'routeToStart':
                str = await this.getString ( data, false, this.current_step );
            break;

            case 'routeToPrevious':
                str = await this.previous ( data );

            break;

            case 'routeToCurrent' :

                switch ( data.type ) {
                    case 'select':
                        str = await this.select ( data, input )
                    break;
                    
                    case 'input':
                        str = await this.input ( data, input )  
                    break;

                }                            
            break;
        }

        return str ;

    } 
    
    async input                   ( data: any, input: any ) {

        //initialize our variables
        let allowNull: boolean = data.canBeEmpty || false;
        let strPrompt: any     = '';
        let str      : any     = '';
        let next     : any     = '';
        let action   : any     = data.action || false;
        
        if ( action && action === 'search' ) {

            //we dont need to validate the input, all we need to do is to perform a search

            let datasetName: any = data ['search-options'].dataset;
            let searchLimit: any = data ['search-options'].limit;
            let saveToName : any = data ['search-options'].saveTo;
            let dataset    : any = this.user_data [ "account-details" ] [ datasetName ] ;

            //perform a search
            let searchResults = dataset.filter ( ( item: any ) => {
                
                let formattedName  = item.label.toLowerCase ().replace ( /\s/g, '' ) ;
                let formattedInput = input.toLowerCase ().replace ( /\s/g, '' ).replace (/[^a-z]/g,'') ;

                if ( formattedName.includes ( formattedInput ) && formattedInput.trim() !== '' ) {
                    return item;
                }

            });

            //error handling
            let error         : string = 'MATCHES_FOUND';
            let overridePrompt: any    = false;

            //No Results
            if ( searchResults.length === 0 ) {
                error = 'NO_MATCH';
            }
            //too many results
            else if ( searchResults.length > 0 && searchResults.length > searchLimit  ) {
                error = 'LIMIT_EXCEEDED';
            }

            switch ( error ) {
                case "MATCHES_FOUND":
                    // go to the next step
                    this.user_data [ "account-details"] [ saveToName ] = searchResults;
                    this.user_data [ "global-request-details"] [ "search_item" ] = input;
                    next                                           = data.next;      

                    this.save ( this.cache_id, this.user_data )

                    let nextData : any = '';
                    if ( next.includes ( 'page' ) ) {
                        nextData = this.loadPage ( next );
                    }
                    else {
                        nextData = this.loadPrompt ( next );
                    }
                    
                    nextData.options = searchResults;

                    str = await this.getString ( nextData, false, next ) ;

                break;

                case "NO_MATCH":
                    //show error prompt for no match
                    overridePrompt = data.errors [ 0 ];
                    this.user_data [ "global-request-details"] [ "search_item" ] = input;
                    this.save ( this.cache_id, this.user_data )
                    str = await this.getString ( data, overridePrompt ) ;

                break;

                case "LIMIT_EXCEEDED":
                    //show error prompt for limit exceeded
                    overridePrompt = data.errors [ 1 ];
                    str = await this.getString ( data, overridePrompt ) ;
                break;
            } 

        }
        else {

            //validate
            let { inputIsValid, failedValidationIndex } = this.validate ( data, input );


            //Show Next Step Prompt on success : input passed validation 
            if ( inputIsValid || allowNull && input.trim().toLowerCase() === this.SKIP_CHARACTER ) {
                
                //persist the next state
                next = data.next; 

                if ( data [ 'format-as'] || false ) {
                    input = this.transform ( data [ 'format-as'], input );
                }

                //persist the inputted data to redis
                if ( data ['save-as'] || false ) {
                    this.user_data [ 'global-request-details' ][ data [ 'save-as' ] ] = input;
                }

                //handling in app authentication by reloading data on valid input
                if ( data.name === 'inapp-login' ) {

                    //refetch the next step data
                    next = this.user_data [ 'inapp-auth-menu'];

                    console.log ( '[ input ]inapp-login next menu = ', next)

                    if ( next.includes ( 'page' ) ) {
                        data.nextData = this.loadPage ( next )
                    }
                    else {
                        data.nextData = this.loadPrompt ( next )
                    }

                }

                str = await this.getString ( data.nextData, false, next ) ;
            }

            /**
             * Show Current Step Prompt on error: input failed validation
             */
            else {
                //NB: failedValidationIndex is the index of the validation error which maps to the key of the error menu in the json component configuration
                // console.log ( { currentStep: this.current_step  } )

                let hasError  = data.error || false
                let hasErrors = data.errors || false
                let error = false;

                if( hasError ){
                    error = data.error;
                }
                if( hasErrors ){
                    error = data.errors [ failedValidationIndex ];
                }

                console.log ( { hasError, hasErrors })
                str = await this.getString ( data, `${error}`, this.current_step ) ;
                strPrompt =  `${error}`;
            } 
        }

        return str;
    }

    async select                  ( data: any, input: any ) {

        let options: any = data.options || false;                 
        let saveAs : any = data ['save-as'] || false;
        let str    : any = '';
        let next   : any = this.current_step;
        let selectAction = data.action || false;

        //input is within range of allowed options
        if ( input > 0 && input <= options.length ) {


            //handle the selects based on their action
            /**
             * Types:
             * key: action
             * 1. Update-parameters ( e.g charges )
             * 2. Transact          ( e.g a confirm prompt )
             * 3. Navigate          ( e.g an transaction status prompt )
             */

            let option        : any = options [ input - 1 ];            
            let nextKey : any = '';
            let nextData: any = '';

            /**
             * -----------------------------
             * 
             *  SELECT
             * 
             * -----------------------------
             */

            //load next data
            if ( data.nextData instanceof Array && data.nextData.length > 0 ) {
                let optionIndex     = input - 1;
                nextData        = data.nextData [ optionIndex ];
                next            = nextData.name; 
                
                //persist the menu chosen incase of the inapp_login_prompt
                if ( nextData.name === 'inapp-login' ) {
                    this.user_data [ 'inapp-auth-menu'] = nextData.next;
                    console.log ( '[select]inapp-login detected loading ', this.user_data [ 'inapp-auth-menu'])
                }
            }
            else {
                nextKey  = data.next;
                nextData = data.nextData;
                next       = nextKey;
            }   

            //get analytics data
            if ( saveAs === false ) { 
                //means that the select is purely for navigation purposes, we can use this to track user choices
                if ( typeof option.name !== 'undefined') {
                    if ( !option.name.includes ( 'page' ) ) {
                        console.log ( `[Analytics] user has accessed the ${option.name}` );
                    }
                    else {
                        console.log ( `[Analytics] user has accessed the ${option.name}` );
                    }
                }
            }

            //get the value that has been entered
            else {
                //persist the inputted data to redis
                this.user_data [ 'global-request-details' ][ data [ 'save-as'] ] = option.value;
            }

            //if cache-local is set to true, cache selection data

            //handle special select actions
            switch ( selectAction ) {
                case 'update-parameters':
                    //TODO: Also add this to the Input handler and search
                    if ( data [ 'external-fetch'] || false ) {

                        let route : string = data [ 'external-fetch'].route || false;
                        let format: any    = data [ 'external-fetch']['format-as'] || false;
                        let api   : any    = data [ 'external-fetch'].api || false;
                        let cache : any    = data [ 'external-fetch'].cache || false;

                        console.log ( '[ Analytics ] Running an external fetch' )

                        let moment = require ( 'moment' )
                        const shortid = require('shortid'); 
                        let ac = new this.analytics ();



                        //fetch the api isht, if the ( can be a basic presentment, or a charges request or any other api sh*t )
                        let api_response : any = {
                            'status':'success',
                            'message' : {
                                'charge': 50,
                                'duty' : 20
                            }
                        }

                        //if responses are required, then we can redirect to another menu on error
                        //examples: presentments for fetching bill amounts
                        if ( api_response.status === 'success' && cache ){
                            
                            console.log ( ' [ Analytics ] caching response values...' );
                            let valuesObject = api_response.message;
                            let valueKeys = Object.keys ( api_response.message );

                            for ( let key of valueKeys ) {

                                let value: any = valuesObject [ key ];

                                if (  format !== 'undefined' ) {
                                    value = this.transform ( format, value );
                                }
                                this.user_data [ 'global-request-details'] [ key ] = value;
                            }
                        }

                        
                        let analyticsTransaction = {
                            timestamp            : `${moment().unix()}`,
                            type                 : 'transaction',
                            name                 : route,
                            charge               : api_response.message.charge,
                            amount               : api_response.message.duty,
                            txid                 : shortid.generate().replace ( /[^a-zA-Z0-9]/g,'80X').toUpperCase(),
                            userid               : this.user_data.msisdn,
                            ussdservice          : this.app_name
                        }
                        ac.enQueue ( analyticsTransaction )
                    }
                break;

                case 'transact':
                    switch ( input ) {

                        case '1':
                            //get analytics data
                            console.log ( `[ Analytics ] user has run an Api call at the end of a transaction...` );

                            let route           : string    = data ['external-fetch'].route;
                            let api_name        : string    = data ['external-fetch'].api;
                            let prompts         : string [] = [
                                data ['external-fetch'].success,
                                data ['external-fetch'].error,
                            ];

                            //formulate our request
                            let request_query = [
                                { name:"walletAccount",value:this.user_data [ "msisdn" ]}
                            ]

                            let query_data = this.user_data ['global-request-details'];
                            let query_data_keys = Object.keys (query_data )

                            for ( let item of query_data_keys ) {
                                let obj = {
                                    name:item,
                                    value:query_data [item ]
                                }

                                request_query.push ( obj )
                            }

                            //fetch data from the API
                            let Api                   = require ( '../api/')
                            let api_connection_params = this.app_config [ 'data-source' ] [ api_name ]
                            let apiHandler            = new Api ( this.api, api_connection_params, api_name )
                            let apiResult             = await apiHandler.run ( route, request_query )

                            //fetch the api isht
                            let api_response : string = 'success';
                
                            //on-success, persist user data
                            if ( apiResult.status === 'success' ) {
                                api_response = 'success';
                            }
                
                            else {
                                api_response = 'failed';
                            }

                            //handle the api response (  it is assumed that it always returns a success or error as the status )
                            let response_map : any = {
                                'success' : 0,
                                "failed"  : 1
                            }

                            //load the next menu data to use to create a menu response string
                            next     = prompts [ response_map [ api_response ] ];
                            let d    : any = this.loadPrompt ( next );
                            nextData = d;
                            
                            //persist to redis
                            let formatApiRoute     = route
                                                    .replace ( /[^a-zA-Z_-]/g, '')
                                                    .replace ( /[_-]/g, ' ')
                                                    .toLowerCase();

                            this.user_data ['global-request-details']['request_name'] = formatApiRoute;

                            let moment = require ( 'moment' )
                            const shortid = require('shortid'); 
                            let ac = new this.analytics ();
                            let analyticsTransaction = {
                                timestamp            : `${moment().unix()}`,
                                type                 : 'transaction',
                                name                 : route,
                                charge               : '',
                                amount               : '',
                                txid                 : shortid.generate().replace ( /[^a-zA-Z0-9]/g,'80X').toUpperCase(),
                                userid               : this.user_data.msisdn,
                                ussdservice          : this.app_name
                            }
                            ac.enQueue ( analyticsTransaction )
                        

                        break;

                        //load the next step which should be the client module
                        case '2':
                            console.log ( `[ Analytics ] user has cancelled the API call`)
                            next = data [ 'on-cancel'] || false;

                            if ( next.includes ( 'page' ) ) {
                                nextData          = this.loadPage ( next );
                            }
                            else {
                                nextData          = this.loadPrompt ( next );
                            }       
                        break;
                    } 
                break;

                case 'navigate':
                    switch ( input ) {

                        //Next step data is already loaded for a basic switch
                        case '1':
                            next       = data [ 'on-accept'];
                            if ( next.includes ( 'page' ) ) {
                                nextData          = this.loadPage ( next );
                            }
                            else {
                                nextData          = this.loadPrompt ( next );
                            } 
                            
                        break;

                        //load the next step which should be the client module
                        case '2':
                            next      = data ['on-cancel'];

                            if ( next.includes ( 'page' ) ) {
                                nextData          = this.loadPage ( next );
                            }
                            else {
                                nextData          = this.loadPrompt ( next );
                            } 
                            if ( next.includes ( 'logout') ) {
                                console.log ( `[ Analytics ] user has logged out`)
                            }
                        break;
                    }                                 
                break;

                case "update-local":
                    if ( input !== '2' ) {

                        let dot       = require ( 'dot-object' );
                        let localPath = data ['local-path'].replace ( /\s/g,'' ) || false;

                        /** 
                         * IMPORTANT! local path has been set to only be two levels deep at maximum
                         * hence data to be set should either be at the root level or on the first
                         * nested level
                         * e.g 
                         * LEVEL 1: language=value OR 
                         * LEVEL 2: account-details>language=value
                         */
                        if ( localPath ) {

                            let localPathArray = localPath.split ( '=' );
                            let path           = localPathArray [ 0 ];
                            let valueKey       = localPathArray [ 1 ];
                            let pathParts      = [];
                            let value = this.user_data [ 'global-request-details'] [ valueKey ];

                            if ( path.includes ( '>' ) ) {
                                pathParts = path.split ( '>' );
                                pathParts = pathParts.filter ( ( p:any ) => {
                                    return p !==''
                                });
                                //update the path with the new value
                                dot.str( pathParts.join ( '.' ), value, this.user_data);
                            }
                            else {
                                this.user_data [ path ] = value
                            }

                            this.user_data = await this.refresh ( this.cache_id, this.user_data );

                        }

                        else {

                        }
                    }                    
                break;
            }

            str = await this.getString ( nextData, false, next ) ;
        }

        //Show Current Step Prompt on error: input not within the range of allowed options                      
        else {           
                        
            str = await this.getString ( data, data.error ) ;
        }

        return str;
    }

    async previous                ( data: any ) {
       let str = await this.getString ( data.previousData,false, data.previousData.name );
       return str;       
    }
    
    async getString               ( data:any, promptOverride:any = false, nextStep:string = '' ) {

        nextStep             = nextStep.trim () === '' ? this.current_step: nextStep;

        //get the prompt
        let menuPrompt  : any = `${data.name}`;

        //overriding a prompt
        if ( promptOverride ) {
            menuPrompt = promptOverride;
        }

        let current_language = this.user_data ["language"];

        let menuString  : any = this.language [ current_language ] [ menuPrompt] + "\n";

        //replace handlePlaceholders
        menuString = this.replace ( menuString ) ;

        //if its a select menu, add the options
        if ( data.type === 'select') {
            //get the select prompt options
            let options: any = data.options; 

            for ( let index in options ) {

                //the options will now be keys defined in the language file
                let optionPrompt  : any = `${options[index].label}`;
                let optionString  : any = this.language [ current_language ]  [ optionPrompt];

                //check if it has an options template for dynamic options
                if ( data [ 'options-template'] ) {
                    let optionsTemplate     : any    = data [ 'options-template'];
                    let optionTemplateString: string = this.language [ current_language ]  [ optionsTemplate ];
                    
                    optionString = optionTemplateString.replace ( /@option/g, optionPrompt );
                }

                if (  typeof ( optionString ) === 'undefined' ) {
                    optionString = optionPrompt;
                }

                menuString +=`${ parseInt ( index) + 1}:${optionString}\n`
            }            
        }

        //add previous option on string if canGoBack is enabled                        
        let canGoBack = data.previous || false ;
        if ( canGoBack ) {
            let previous_prompt = this.language [ current_language ]  [ "previous"];
            menuString +=`${this.PREVIOUS_CHARACTER}:${previous_prompt}\n`;
        }

        //home menu
        // let home_prompt = this.language [ current_language ] [ "home" ];
        // menuString +=`000:${home_prompt}\n`;

        //user_data
        this.user_data [ 'current_step' ] = nextStep;
        let response = await this.save ( this.cache_id, this.user_data );

        //return the menu string
        return menuString ;        
    }

    replace                       ( menuString:string ) : string { 

        let menuStringArray: string[] = menuString.split ( ' ' );

        let items = menuStringArray.filter ( ( item:string ) => {            
            if ( item.includes ( '@' ) ) {                
                return item;
            }
        });

        for ( let item of items ) {

            let formatItem   = item.replace ( /[^a-zA-Z_-]/g,'' );
            
            let searchResult = this.searchData ( formatItem );
            
            if ( typeof searchResult !== 'undefined' ) {
                let trimmedItem = `${item}`
                                    .replace ( /\s/g,'' )
                                    .replace ( / /g, '' )
                                    .replace ( /[^@a-zA-Z_-]/g,'' );

                let rgx         = new RegExp ( trimmedItem, 'g');
                menuString      = menuString.replace ( rgx, searchResult );                
            }
        }

        return menuString;
    }
    
    setStartMenu                  () {
        /**
         * -----------------------------------------------------------------------
         *  GETTING THE MODULE TO PARSE ON INITIAL ACCESS
         * 
         *  Once the conditional logic is complete, it sets `this.current_menu` 
         *  to the appropriate menu globally
         * 
         *  App config file has to be loaded
         * 
         * -----------------------------------------------------------------------
         */
        

        try { 

            let initialStep       : any = '';
            let page_switch_config: any = this.app_config [ "page-switch-check" ];
            let switchParam       : any = this.app_config [ "page-switch-check" ].name;
            let initital_page_type: any = this.user_data  ["account-details"] [ switchParam ];
            let initial_page      : any = page_switch_config.options [ initital_page_type ].page;
            let module_is_enabled : any = page_switch_config.options [ initital_page_type ].enabled;

            if ( module_is_enabled ) {

                //let initial step to load is the enabled module based on the users type from the user data
                initialStep = `${initial_page}`.replace ( /_/g, '-' );
                
                /**
                 * Hence The Logic is : 
                 * 
                 *  Registration enabled : 
                 *       - If the user is registered: 
                 *          If Authentication is enabled :
                 *          - check if the account is blocked: If blocked, show the account blocked prompt.
                 *          - check the number of pin trials ( if account is not blocked and pin trials is zero, reset the pin trials and show the
                 *            authentication prompt ) else show the authentication prompt with the number of pin trials remaining
                 *          If Authentication is disabled :
                 *          - load the initial module prompt
                 *      - If the user is not registered:
                 *          - Show the registration module prompt
                 *  Registration disabled :
                 *      - load the initial module prompt
                 */
                let registrationEnabled  : any = this.app_config [ "register" ];
                let registerParam        : any = this.app_config [ "registration-check"  ];
                let isRegistered         : any = this.user_data ["account-details"] [ registerParam ];
                let initialLoginCheck    : any = this.app_config [ "first-login-check"  ];
                let isFirstLogin         : any = this.user_data ["account-details"] [ initialLoginCheck ];
                let blockedAccessParam   : any = this.app_config   [ "blocked-account-check" ];
                let isBlocked            : any = this.user_data    ["account-details"] [ blockedAccessParam ]; 
                let maxPinTrials         : any = this.app_config   [ "pin-trials-max"  ];
                let pinTrialsRemaining   : any = parseInt ( this.user_data    [ "pin-trials-remaining" ], 10 );
                let authenticationEnabled: any = this.app_config [ "authenticate" ]; 

                //registered account with authentication enabled
                if ( registrationEnabled && isRegistered && authenticationEnabled ) { 

                    //user is accessing the app for the first time
                    if ( isFirstLogin ) {
                        initialStep = `first-login-system-pin`;
                    }
                    
                    else {

                        //user account is active
                        if ( pinTrialsRemaining === 0 ) {
                            this.user_data [ "pin-trials-remaining" ] = maxPinTrials;
                            initialStep = `login`;
                        }

                        //TODO: reset the pin trials to max if the user provides the correct password before using up all available pin trial attempts
                        else if ( pinTrialsRemaining > 0 && pinTrialsRemaining < maxPinTrials  ) {
                            initialStep = `login_wrong_last_pin_prompt`;
                        }

                        else if ( pinTrialsRemaining === maxPinTrials ) {
                            initialStep = `login`;
                        }
                    }
                }

                //unregistered account
                if ( registrationEnabled && !isRegistered ) {
                    initialStep = `registration-page`;

                }

                //blocked accounts
                if ( isBlocked ) {
                    initialStep = `account-blocked`;
                }

                //set the current step
                this.current_step = initialStep;

            }
            else {
                //initial module is disabled
                console.log ( 'initial module is disabled');
                this.current_step = `error`;
            }  
            
            
        }
        catch ( e ){
            //initial module could not be loaded
            console.log ( 'initial module could not be loaded' );
            this.current_step = `error`;
        } 

        console.log ( { current_step:this.current_step})
    }





    //Loading of data seems to be working so far :
    loadData                      () { 
        let data: any = {};
        
        if ( this.current_step.includes ( 'page' ) ) {
            data                        = this.loadCurrentData  ( this.current_step, 'page' );
            data [ 'previousData']      = this.loadPreviousData ( this.current_step, 'page' );
            data [ 'nextData']          = this.loadNextData     ( this.current_step, 'page' );
        }

        else {            
            data                        = this.loadCurrentData  ( this.current_step );
            data [ 'previousData']      = this.loadPreviousData ( this.current_step );
            data [ 'nextData']          = this.loadNextData     ( this.current_step );
        }

        return data;
    }

    loadPage                      ( page_name: any ) :any {

        let data      : any = false;

        try {
            //load the page data
            let page_data = this.pages [ page_name ] || false;

            if ( page_data ) {

                //add only the children that are enabled                
                let options : any = page_data.options;//TODO: add a check here for empty or malformed page options

                options = options.filter ( ( option: any ) => {
                    return option.enabled !== false;
                });

                page_data.options = options;  
                
                data = page_data;    
            }                 

        }
        catch ( e ) {
            console.log ( 'failed to load page' );
        }

        return data;
    }   

    loadPrompt                    ( prompt_name:any  ) :any {

        let data     : any    = false;
        
        try {
            //get cache file and prompt key
            let prompts_lookup   : any = this.prompts_cache;
            let prompt_groups    : any = Object.keys ( prompts_lookup );
            let prompt_group_name: any = '';

            //loop through the cache items
            for ( let key of prompt_groups ) {
                
                let prompts = prompts_lookup [ key ];
                
                for ( let prompt of prompts )  {
    
                    if ( prompt === prompt_name ) {
                        prompt_group_name = key;
                        break;
                    }
                } 
                
            }

            //load the prompt data           
            let prompt_data        : any = this.prompts [ prompt_group_name ];


            if ( prompt_data instanceof Array ) {
                for( let index in prompt_data ) {
                    if ( prompt_data [ index ].name  === prompt_name ) {                       
                        data = prompt_data [ index ];                        
                    }
                }
            }
            else {
                data = prompt_data;
            }

            //replace with the actual child objects in case the options variable is a string reference
            let hasOptions = data.options || false;

            if ( hasOptions && typeof hasOptions === 'string' ) {
                data.options = this.user_data [ 'account-details'] [ data.options ] || this.user_data [ 'global-constants'] [ data.options ]
            }
            
        }
        catch ( e ) {
            console.log ( ` [ Error ] unable to load the prompt data for \`${prompt_name} - msg: ${e.message}` );
        }

        return data;
    }

    loadRootPrompt                ( prompt_group_name: any ): any {

        // console.log ( { promptsRootname: name })

        //get the components Data:
        let prompt_data: any = this.prompts [ prompt_group_name ];
        let data       : any = []

        if ( prompt_data instanceof Array ) {
            data = prompt_data [ 0 ]
        }
        else {
            data = prompt_data;
        }

        // console.log ( { promptsRootData: prompt_data })

        //replace with the actual child objects in case the options variable is a string reference
        if ( typeof ( data.options ) === 'string' && data.type === 'select' ) {
            let children = this.user_data [ 'account-details'] [ data.options ] || this.user_data [ 'global-constants'] [ data.options ]
            data.options = children;
        }  

        return data;

    }

    loadCurrentData               ( current_step_name:any, currentType: any = '') :any {

        let data      : any    = false; //is either an object or false
        // console.log ( `Loading Current Step Data for ${name} of type ${currentType}` );

        switch ( currentType ) {

            //The previous step of a module is either another module or disabled
            case 'page': 
                try {                
                    data = this.loadPage ( current_step_name );
                }
                catch ( e ) {
                    console.log ( 'failed to load the page' );
                }
            break;
            
            //The previous step of a component may either be another component or a module or disabled
            default:
                try {                    
                    data = this.loadPrompt ( current_step_name );
                }
                catch ( e ) {
                    console.log ( 'failed to load the prompt ( s )' );
                }            
            break;
        }     
        // console.log ( { currentStepData: data, } );
        return data;        
    }

    loadPreviousData              ( current_step_name:any, currentType: any = '') :any {

        let data      : any    = false; //is either an object or false

        switch ( currentType ) {

            //The previous step of a page is either another page or disabled
            case 'page':
                try {
                    //hence get current Module data and use it to determine the previous module data, then load that as the final data                  
                    let currentData = this.loadPage ( current_step_name );
                    let canGoBack = currentData.previous || false

                    if ( canGoBack ) {

                        let previous = currentData.previous;
                        data         = this.loadPage( previous );
                    }                    
                }
                catch ( e ) {
                    console.log ( 'failed to load the previous step of the page' );
                }
            break;
            
            //The previous step of a prompt may either be another prompt or a page or disabled
            default:
                try {
                    let currentData = this.loadPrompt ( current_step_name );
                    let canGoBack = currentData.previous || false

                    if ( canGoBack ) {

                        let previous = currentData.previous;

                        //previous is a page
                        if ( previous.includes ( 'page' ) ) {
                            data = this.loadPage ( previous );
                        }

                        //previous step is a prompt
                        else {
                            data = this.loadPrompt ( previous );
                        }
                    }
                }
                catch ( e ) {
                    console.log ( 'failed to load previous step of the prompt' );
                }            
            break;
        }     

        // console.log ( { previousStepData : data } );
        
        return data;
    }

    loadNextData                  ( current_step_name:any, type: any = '') :any {

        let data      : any    = false; //is either an array, an object or false


        //for a page, next step is an array of pages and/or prompt data
        if ( type === 'page' ) { 
            try {
                let currentData  : any   = this.loadPage ( current_step_name );
                // console.log ( { currentData } )
                let options     : any   = currentData.options; //TODO: check to ensure children are iterable

                //filter the children to get only the enabled children
                options  = options.filter ( ( option:any ) => {
                    if ( option.enabled ) {
                        return option;
                    }
                })

                //get the next step array
                let nextArray: any[] = options.map ( ( option: any ) => {

                    let name         = option.name;
                    let authenticate = option.authenticate ||false;

                    if ( authenticate ) {

                        let auth = Object.assign ( {}, this.loadPrompt ( 'inapp-login' ) );

                        if ( name.includes ( 'page') && option.enabled ) {
                            let data =  this.loadPage ( option.name );
                            auth.next = data.name;
                            return auth;
                        }

                        //load prompt data
                        if ( !name.includes ( 'page') && option.enabled ) {
                            //load the root element of the component
                            let data =  this.loadRootPrompt ( option.name ) ;
                            auth.next = data.name;
                            return auth;
                        }
                        return auth;
                    }
                    else {
                        if ( name.includes ( 'page') && option.enabled ) {
                            return this.loadPage ( option.name );
                        }

                        //load component element data
                        if ( !name.includes ( 'page') && option.enabled ) {
                            //load the root element of the component
                            return this.loadRootPrompt ( option.name ) ;
                        }
                    }

                });
                
                data = nextArray;
            }
            catch ( e ) {
                console.log ( 'failed to load the next page ' + current_step_name, e );
            }
        }

        //for a prompt, the next step is another prompt or a page or false
        else {
            try {

                let currentData: any = this.loadPrompt ( current_step_name );
                let next       : any = currentData.next || false;

                if ( next ) {

                    if ( next.includes ( 'page' ) ) {
                        data = this.loadPage ( next );
                    }
                    else {
                        data = this.loadPrompt ( next );
                    }                        
                }
            }
            catch ( e ) {
                console.log ( 'failed to load the next prompt ( s )' );
            } 
        }    
        
        return data;
    }

    searchData                    ( needle: any ) {

        let haystack        : any = this.user_data;
        
        let searchObj : any = ( object:any , key:any ) => {

            let value: any;

            Object.keys( object ).some( ( k ): any => {

                if ( k === key ) {
                    value = object [ k ];
                    return true;
                }

                if ( object [ k ] && typeof object [ k ] === 'object' ) {
                    value = searchObj ( object [ k ], key );
                    return value !== undefined;
                }
            });

            return value;
        };

        let result = searchObj ( haystack, needle );        
        return  result;
        
    }
    


    //Other operations:    
    async save                    ( key:string, data: any ) {
        let cache   : any = require ( '../cache/cache' ); 
        let store   : any = new cache ();
        let response: any = await store.put ( key, data );

        return response;
    }

    async refresh                 ( key:string, data: any ) {
        let cache   : any = require ( '../cache/cache' ); 
        let store   : any = new cache ();
        let response: any = await store.put ( key, data );
        let result  : any = await store.get ( key );

        return result;
    }
    
    validate                      ( data:any, input:any ) {


        const joi       : any = require('joi');
        let inputIsValid: any = false;


        /**
            * --------------------------------------------------------------
            *  VALIDATE USER INPUT
            * 
            *  NB: `For multiple validations e.g isAmount:min=50|isNumber, we should 
            *  have a variable that stores whether all validations are passed and
            *  if any validation has not passed, then the appropriate errorn msg
            *  should be returned. This will enable us to have multiple custom error
            *  messages based on each failed validation e.g minimum amount not reached,
            *  invalid amount, ...etc
            *  
            * 
            * --------------------------------------------------------------
            */

        //perform validations here using a for loop
        //Structure of the validation is ~ name, type, arguments
        let validations      : any = data.validation;
        let currentValidation: any = '';


        //run the validations
        for ( let validation of validations ) {

            let validatorName        :any = validation.name;
            let validationArguments  :any = [];
            currentValidation             = validatorName;
            let validationType       :any = validation.type;

            //if the validation contains arguments, get the validation arguments
            if ( typeof ( validation.arguments ) !== 'undefined' ) {

                let validationArgumentsArray  = validation.arguments.split ( "," ) ;
                validationArgumentsArray  = validationArgumentsArray.filter ( ( e : any ) => {
                    return e && e != null && e!= 'null' && e.trim() != '' && typeof e != 'undefined'
                });        

                for ( let argument of validationArgumentsArray ) {

                    if ( argument.includes ( "=" ) ) {
                        let argumentsParts = argument.split ( "=" );
                        validationArguments.push ( argumentsParts [ 1 ] );
                    }

                }
            }  
            
            //get validator path
            let validationRules    : any    = false;
            let schema             : any    = {};        

            // perform custom validations or joi validations or api calls here
            try {
                let validators = require ( './validators' )
                validationRules = validators [ validatorName ];

                if ( validationType       === 'joi'     ) {
                
                    //validate
                    schema         = joi.object().keys( validationRules ( joi, ... validationArguments )  ).unknown(); 
                    const validate = joi.validate( { [ validatorName ] : input }, schema ); 

                    if ( validate.error === null ) {
                        inputIsValid = true;
                    }
                    else {
                        inputIsValid = false;
                        break;
                    }
                    
                }

                else if ( validationType  === 'custom'  ) {
                    let validate = validationRules ( input, this.user_data );

                    if ( validate ) {
                        inputIsValid = true;
                    }
                    else {
                        inputIsValid = false;
                        break;
                    }                        
                }

                else if ( validationType  === 'api'     ) {
                }                
            }
            catch ( e ) {
                console.log ( e );
            }
        }

        //get the current index of the failed validation
        let failedValidationIndex : any = false;
        if ( inputIsValid === false ) {

            for ( let index in validations ) {
                if ( validations [ index ].name.includes ( currentValidation ) ) {
                    failedValidationIndex = index;
                }
            }

        }

        return { 
            inputIsValid , 
            failedValidationIndex 
        }
    }

    transform                     ( id: any, value: any ) {

        let transformed: any = value;

        switch ( id ) {
            case 'money':
                transformed = `${parseFloat ( value ).toFixed ( 2 )}`
            break;

            case 'capitalize':
                transformed = value.toLowerCase().replace(/(?:^|\s)\S/g, function(a:any) { return a.toUpperCase(); });
            break;

            case "smep-pin-hash":
                let CryptoJS     = require ("crypto-js");
                transformed = CryptoJS.HmacSHA256( Buffer.from ( value + this.user_data.msisdn ).toString('base64') , "secret").toString(CryptoJS.enc.Hex);
            break;
        }

        return transformed;
    }
}

module.exports = MenuHandler;