class AnalyticsClient {

    private Queue : any = require('bull');
    private queue : any = new this.Queue("analytics_queue");

    constructor ( ) {
    }

    enQueue ( transaction: any  ) {
        this.queue.add  ( transaction )
        this.queue.close();
    }

}

module.exports = AnalyticsClient;