/**
 * Redis CRUD class
 * @author: JMbuguah
 * Date: 14-06-2017:1226hrs
 * @description: Class that performs Redis Crud operations
 */
class Store {

    private env       : any = require ( './env' ) 
    private connection: any = {}

    constructor ( ){ 
        
        switch ( this.env.environment ) {
            
            case 'dev':
                this.connection     = this.env.cache.dev;
            break;

            case 'staging':
                this.connection     = this.env.cache.staging;
            break;
        
            case 'prod':
                this.connection     = this.env.cache.prod;
            break;
        }
        
    }

    connect (){

        const redis : any = require('redis');
        var client  : any = redis.createClient( this.connection.port, this.connection.host );
        
        client.on( 'connect', () => {
            // console.log("[Connected] Redis Client on port %d,",this.connection.port," host:",  this.connection.host );
        });

        client.on( 'end', () => {
            // console.log("[Terminated] Redis Client");
        });

        return client;
    }

    close( client: any ){
        client.quit();
    }

    createHash( client: any, id: string, data: any ) : Promise <string> {

        return new Promise ( ( resolve, reject ) => {

            client.select( this.connection.database , ( err: any, res : any ) => {

                if ( err ) reject ( err );   
                       
                client.hmset( id, data, ( err: any, res: any ) => {
                    if ( err ) reject ( err );
                    return resolve(res);
                });
            });

        });
    }

    readHash( client: any, id: string )  : Promise <string> {

        return new Promise ( ( resolve, reject ) => {
       
             client.select( this.connection.database , ( err: any, res: any ) => {

                if ( err ) reject ( err );
                
                client.hgetall( id, ( err:any, data: any ) => {
                    if ( err ) reject ( err );
                    return resolve(data);
                });
             });

        });

    }

    deleteHash( client: any, id : any )  : Promise <string> {
        
        return new Promise ( ( resolve, reject ) => {

            client.select( this.connection.database , ( err:any, res: any ) => {

                if ( err ) reject ( err );

                client.del( id, ( err:any, res:any ) => {

                    if( err ) reject ( err );
                    resolve ( res );
                }); 
            });
        });
    }

    readMultipleHashes( client: any, keys : any )  : Promise <string> {     

        return new Promise((resolve, reject) => {

            client.select ( this.connection.database, ( err: any , res: any ) => {

                if ( err ) reject ( err );

                client = client.multi({pipeline: false});

                keys.forEach( ( key: any, index: any  ) => {
                    client = client.hgetall ( key )
                })

                client.exec( ( err: any, res: any ) => {
                    if ( err ) reject ( err )
                    return resolve ( res );
                })

            })

        })

    }

    keyExists( client: any, id: any )  : Promise <boolean> {

        return new Promise ( ( resolve, reject ) => {

            client.select( this.connection.database , ( err : any, res : any ) => {

                if ( err ) reject ( err );
                client.exists( id, function( err: any, reply: any) {
                    if (reply === 1) {
                        resolve ( true );
                    } else {
                    resolve (false);
                    }
                });
            });
         });
    }

}

//export the Redis Class
module.exports = Store;