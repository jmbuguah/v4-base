class Cache {      

    private connection: any = {
        driver             : "redis",
        host               : "localhost",
        port               : "6379",
        database           : 0
    };

    private redis     : any = require ( './redis' ); ;

    constructor  () {}
    
    //crud methods
    async put ( userId : any, data : any )       : Promise<boolean>  {
        
        data               = this.valuesToJson ( data );

        //create the data then close the connection
        let redis    = new this.redis ( this.connection );
        let client   = redis.connect ();
        let response = await redis.createHash( client, userId, data );        
        redis.close ( client ) ;

        if ( response === 'OK' ) {
            response = true;
        }
        else {
            response = false;
        }

        return response;
        
    }

    async get ( userId : any )        : Promise<any>   {

        //create the data then close the connection
        let redis    = new this.redis ( this.connection );
        let client   = redis.connect ();
        let response = await redis.readHash( client, userId );   
        
        //close our connection
        redis.close ( client ) ;

        if ( response ) {
            response = this.valuesFromJson ( response ) ;
        }
        else {
            response = false;
        }
        return response;
    }

    async getMany ( keys : any )        : Promise<any>   {

        //create the data then close the connection
        let redis    = new this.redis ( this.connection );
        let client   = redis.connect ();
        let response = await redis.readMultipleHashes(client, keys);

        //close our connection
        redis.close ( client );

        if (response) {

            response = response.map ( ( name: any ) => {
                return this.valuesFromJson( name );
            })

            let obj : any = {}

            keys          = keys.map ( ( key: any ) => {
                let arr   = key.split( ':' )
                return arr [ arr.length -1 ]
            })

            for ( let index in keys) {
                obj [ keys [ index ] ] = response [ index ]
            }
            
            response = obj;
        }
        else {
            response = false;
        }
        return response;
    }

    async delete ( userId : any )     : Promise<any>   {

        //create the data then close the connection
        let redis    = new this.redis ( this.connection );
        let client   = redis.connect ();
        let response = await redis.deleteHash( client, userId );        
        redis.close ( client ) ;

        if ( response === 0 ) {
            return false;
        }

        if ( response === 1 ) {
            return true;
        }
       
        return response;
    }

    async exists ( userId : any )     : Promise<any>   {

        //create the data then close the connection
        let redis    = new this.redis ( this.connection );
        let client   = redis.connect ();
        let response = await redis.keyExists( client, userId );        
        redis.close ( client ) ;
        return response;
    }

    //helper functions
    valuesToJson  ( data : any )    : object {

        
        let jsonObj: any   = {};

        try {
            let keys   : any[] = Object.keys ( data );
            for ( let key of keys ) {
                jsonObj [ key ] = JSON.stringify ( data [ key ] ); 
            }
        }

        catch ( e ) {

        }

        return jsonObj;

    }

    valuesFromJson  ( data : any )  : object {
        
        let jsonObj: any   = {};

        try {
            let keys   : any[] = Object.keys ( data );

            for ( let key of keys ) {
                jsonObj [ key ] = JSON.parse ( data [ key ] ); 
            }
        }

        catch ( e ) {

        }


        return jsonObj;

    }
}

module.exports = Cache;